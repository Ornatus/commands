<h3>****E-mail automático, não responda****</h3>

<p>Olá Franqueado <b><?php echo $orders[0]->brazil_store_name;?></b>, esse é um email automático gerado pelo sistema de Gestão da Franqueadora.<br>
Abaixo, segue pedido da LV pendente para liberação. Importante se atentar ao prazo limite para tal:</p>

<table border="1" style="width: 100%; border: 1px solid #CCC; border-collapse: collapse;" class="table table-bordred table-striped">
    <thead>
        <tr>
            <th style="width: 25%; text-align:left; padding-left: 5px;"><H5><b>#Pedido</b></H5></th>
            <th style="text-align:right; padding-right: 5px;"><H5><b>Dias Pendentes</b></H5></th>
            <th style="text-align:right; padding-right: 5px;"><H5><b>Qtde. Peças</b></H5></th>
            <th style="text-align:right; padding-right: 5px;"><H5><b>Valor Pedido</b></H5></th>
        </tr>
    </thead>
    <tbody>
        <?php
            $totalPecas = 0;
            $totalPedido = 0;
            
            $i = 0;
            foreach ($orders as $order) {
                $totalPecas += $order->pecas;
                $totalPedido += $order->valor_pedido;
        ?>
                <tr style="background-color: <?php echo ($i % 2 == 0 ? '#DCDCDC' : '#FFF'); ?>;">
                    <td style="padding-left: 5px;"><?php echo $order->lv_order_id; ?></td>
                    <td style="text-align:right; padding-right: 5px;"><?php echo $order->dias; ?></td>
                    <td style="text-align:right; padding-right: 5px;"><?php echo $order->pecas; ?></td>
                    <td style="text-align:right; padding-right: 5px;">R$<?php echo number_format($order->valor_pedido, 2, ',', '.'); ?></td>
                </tr>
        <?php
            $i++;    
        }
        ?>
        <tr>
            <td colspan="2" style="text-align: right; font-weight: bold; padding: 5px;">TOTAL:</td>
            <td style="text-align: right; font-weight: bold; padding: 10px 5px;"><?php echo $totalPecas; ?></td>
            <td style="text-align: right; font-weight: bold; padding: 10px 5px;">R$<?php echo number_format($totalPedido, 2, ',', '.'); ?></td>
        </tr>
    </tbody>
</table>

<p>Após esse período, senão liberado, o pedido será cancelado e as peças retornadas para compra na LV a toda a rede. Neste caso, será necessário realizar nova reserva.</p>

<h3>****E-mail automático, não responda****</h3>
