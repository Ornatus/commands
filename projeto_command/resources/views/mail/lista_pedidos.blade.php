<h3>****E-mail automático, não responda****</h3>

<h2>Olá Franqueado <?php echo $brazil_store_name?></h2>


<p>Segue abaixo o resumo de produtos disponiveis na caixa da sua loja.</p>
<p>O volume apresentado já descarta os pedidos enviados para liberação</p>

<b>Total de Peças:</b> <?php echo $total_products;?>
<br>
<b>Valor Total:</b> R$<?php echo $total_price;?>
<br>


<br>
<b>PEDIDOS NA CAIXA DESDE:</b> <?php echo date("d/m/Y", strtotime( $data_min)); ?> - <?php echo $text_date;?>
<br>
<div class="table-responsive" id="lista-pedidos">
  <table id="table_pedidos" class="table" style="font-size:9pt" border=1>
    <thead>
      <tr>
        <th scope="col">PEDIDO/SEMANA</th>
        <th scope="col">PLATAFORMA</th>
        <th scope="col">DATA</th>
        <th scope="col">QUANTIDADE<br>PEÇAS</th>
        <th scope="col">VALOR<br>TOTAL</th>
        <?php foreach($colunas as $coluna){ ?>
          <th scope="col"><?php echo $coluna; ?></th>
        <?php } ?>

      </tr>
    </thead>
    <tbody>
      <?php foreach($pedidosCsm as $key=>$pedido){
        ?>
        <tr>
          <th scope="row" align="left"><?php echo $key; ?></th>
          <th scope="row" align="left">CSM</th>
          <th scope="row" align="left"><?php echo $pedido['data'] ?></th>
          <th scope="row" align="left"><?php echo $pedido['total'] ?></th>
          <th scope="row" align="left">R$<?php echo number_format($pedido['preco_total'], 2, '.', '')  ?></th>
          <td><?php echo isset($pedido['ERR']) ? $pedido['ERR']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['NCL']) ? $pedido['NCL']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['BRC']) ? $pedido['BRC']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['RNG']) ? $pedido['RNG']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['SET']) ? $pedido['SET']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['ANK']) ? $pedido['ANK']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['PDT']) ? $pedido['PDT']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['OUTROS']) ? $pedido['OUTROS']->quantidade : 0; ?></td>

        </tr>
      <?php  } ?>

      <?php foreach($pedidosPpm as $key=>$pedido){
        ?>
        <tr>
          <th scope="row" align="left"><?php echo $key; ?></th>
          <th scope="row" align="left">PPM</th>
          <th scope="row" align="left"><?php echo $pedido['data'] ?></th>
          <th scope="row" align="left"><?php echo $pedido['total'] ?></th>
          <th scope="row" align="left">R$<?php echo number_format($pedido['preco_total'], 2, '.', '')  ?></th>
          <td><?php echo isset($pedido['ERR']) ? $pedido['ERR']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['NCL']) ? $pedido['NCL']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['BRC']) ? $pedido['BRC']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['RNG']) ? $pedido['RNG']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['SET']) ? $pedido['SET']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['ANK']) ? $pedido['ANK']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['PDT']) ? $pedido['PDT']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['OUTROS']) ? $pedido['OUTROS']->quantidade : 0; ?></td>

        </tr>
      <?php  } ?>

      <?php foreach($pedidosLv as $key=>$pedido){
        ?>
        <tr>
          <th scope="row" align="left"><?php echo $key; ?></th>
          <th scope="row" align="left">LOJAVIRTUAL</th>
          <th scope="row" align="left"><?php echo $pedido['data'] ?></th>
          <th scope="row" align="left"><?php echo $pedido['total'] ?></th>
          <th scope="row" align="left">R$<?php echo number_format($pedido['preco_total'], 2, '.', '')  ?></th>
          <td><?php echo isset($pedido['ERR']) ? $pedido['ERR']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['NCL']) ? $pedido['NCL']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['BRC']) ? $pedido['BRC']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['RNG']) ? $pedido['RNG']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['SET']) ? $pedido['SET']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['ANK']) ? $pedido['ANK']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['PDT']) ? $pedido['PDT']->quantidade : 0; ?></td>
          <td><?php echo isset($pedido['OUTROS']) ? $pedido['OUTROS']->quantidade : 0; ?></td>

        </tr>
      <?php  } ?>

    </tbody>
  </table>

  <h3>****E-mail automático, não responda****</h3>

</div>





<script>

$(function() {
  $("#table_pedidos").tablesorter({



  });
});


</script>
