<h3>****E-mail automático, não responda****</h3>
<br>

<h2>Olá Franqueado <?php echo $info->brazil_store_name; ?></h2>

<p>Informamos que a Prioridade <?php echo $info->priority; ?> do CSM <?php echo $info->news_week_id; ?> (Período entre <?php echo date("d/m/Y", strtotime($info->data_inicio_csm)); ?> à <?php echo  date("d/m/Y", strtotime($info->data_fim_csm)); ?>) terá sua divulgação nas mídias a partir de <?php echo  date("d/m/Y", strtotime($info->data_inicio_midia)); ?>.
<br>
<br>
Notamos que você ainda não liberou o pedido <?php echo $info->order_id; ?>.
<br>
<br>
Pelo estudo de tempo de frete que temos de sua região, seu prazo máximo de liberação em tempo hábil de chegada para exposição em loja será dia <?php echo date("d/m/Y", strtotime($info->data_limit)); ?> - 5 dias.
<br>
<br>
<b>Libere assim que possível! </b>
<br>
<br>
Lembramos que a sincronia de produto em loja x divulgação de Mkt, gera aumento da velocidade de giro de venda da peça em loja "EFEITO TURBO", e atende uma expectativa de sua cliente local que acompanha nossas redes sociais.
Boas vendas! Equipe Mkt/Produto.</p>

<br>
<h3>****E-mail automático, não responda****</h3>
