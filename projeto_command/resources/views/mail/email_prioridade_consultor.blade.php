<h1>Olá, <?php echo $supervisor->email;?></h1>


<p>
  Segue abaixo a lista de lojas que estão com pedidos da prioridade <b><?php echo $info_news_week_priority[0]->priority; ?></b> da semana <b><?php echo $info_news_week_priority[0]->news_week_id; ?></b> na caixa.
  <br>
  O Início da Mídia desta prioridade é <b><?php echo date("d/m/Y",strtotime($info_news_week_priority[0]->date_start)); ?></b>
</p>


<table width="100%" border=1 class="table table-bordred table-striped">
  <thead>
    <th style=" text-align:left"><H5><b>Pedido</b></H5></th>
    <th style=" text-align:left"><H5><b>Loja</b></H5></th>
    <th style=" text-align:left"><H5><b>Região</b></H5></th>
    <th style=" text-align:left"><H5><b>Prioridade</b></H5></th>
    <th style=" text-align:left"><H5><b>Peças Pedidas</b></H5></th>
    <th style=" text-align:left"><H5><b>Status</b></H5></th>
    <th style=" text-align:left"><H5><b>Total Preço <br> Pedido</b></H5></th>
    <th style=" text-align:left"><H5><b>Data de Pedido <br> Liberação</b></H5></th>
    <th style=" text-align:left"><H5><b>Data Limite <br> para Liberação</b></H5></th>
    <th style=" text-align:left"><H5><b>Data Início</b></H5></th>
    <th style=" text-align:left"><H5><b>Data Fim</b></H5></th>

  </thead>
  <tbody>

    <?php foreach( $orders as $order ) { ?>
      <tr >
        <td style="text-align:left"><h6> <?php echo $order->order_id; ?> </h6></td>
        <td style="text-align:left"><h6> <?php echo $order->brazil_store_name; ?> </h6></td>
        <td style="text-align:left"><h6> <?php echo $order->descricao; ?> </h6></td>
        <td style="text-align:left"><h6> <?php echo $order->invoice_no; ?> </h6></td>
        <td style="text-align:left"><h6> <?php echo $order->qtd_products; ?> </h6></td>
        <td style="text-align:left"><h6>
          <?php switch ($order->order_status_id) {
            case 1:
            echo "Pendente";
            break;
            case 2:
            echo "Em Separação";
            break;
            case 3:
            echo "Disponível na Caixa";
            break;
            case 8:
            echo "Faturado";
            break;
          } ?>
        </h6></td>

        <td style="text-align:left"><h6> R$ <?php echo number_format($order->total, 2, ',', '.');?> </h6></td>
        <td style="text-align:left"><h6> <?php echo (strtotime($order->data_add)) ? date("d/m/Y",strtotime($order->data_add)) : ''; ?> </h6></td>
        <?php
        $color = "";
        $date_limit = "";
        $dias_frete = ($order->dias_frete) ? $order->dias_frete : 0 ;
        $current_date = date('Y-m-d');

        $date = new DateTime($order->date_start);
        $date->modify('-'.$dias_frete.' day');
        $date_limit = $date->format('Y-m-d');

        if(isset($order->lv_confirm_picking_id) && $order->lv_confirm_picking_id > 0 ){
            $current_date = $order->data_add;
        }

        if($date_limit < $current_date)
        {
          //vermelho
          $color = "#ff4d4d";
        }
        elseif($date_limit == $current_date )
        {
          //amarelo
          $color = "#ffff1a";
        }


        ?>

        <td style="text-align:left; background-color:<?php echo $color;?>">
          <h6> <b><?php echo date("d/m/Y",strtotime($date_limit));?></b> </h6>
        </td>
        <td style="text-align:left"><h6> <?php echo $order->date_start_formated;?> </h6></td>
        <td style="text-align:left"><h6> <?php echo $order->date_end_formated;?> </h6></td>
      </tr>
    <?php } ?>
  </tbody>
</table>
