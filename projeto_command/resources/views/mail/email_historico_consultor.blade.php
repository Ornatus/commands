<h3>****E-mail automático, não responda****</h3>

<h2>Olá! <?php echo $consultor; ?> (<?php echo $email; ?>) </h2>
<p>Segue histórico das suas lojas.</p>
<br>
<br>
<div class="table-responsive" id="lista-pedidos">
  <table id="table_pedidos" class="table" style="font-size:9pt" border=1>
    <thead>
      <tr>
        <th scope="col">AREA</th>
        <th scope="col">LOJA</th>
        <th scope="col">CONSULTOR</th>
        <th scope="col">QUANTIDADE<br>PEÇAS NA CAIXA</th>
        <?php  foreach ($semanas as $semana) { ?>
          <th scope="col">CSM <?php echo $semana->news_week_id; ?> </th>
        <?php } ?>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($caixa as $cx) {?>
    <tr>
      <td><?php echo $cx['area']; ?></td>
      <td><?php echo utf8_decode($cx['loja']); ?></td>
      <td><?php echo utf8_decode($cx['supervisor']); ?></td>
      <td><?php echo $cx['caixa']; ?></td>
      <?php  foreach($semanas as $semana) { ?>
        <td><?php echo $cx[$semana->news_week_id]; ?></td>
      <?php  } ?>
    </tr>
  <?php } ?>
    </tbody>
  </table>

  <h3>****E-mail automático, não responda****</h3>

</div>
