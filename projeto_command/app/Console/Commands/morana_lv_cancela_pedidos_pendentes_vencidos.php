<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class morana_lv_cancela_pedidos_pendentes_vencidos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:morana-lv-cancela-pedidos-pendentes-vencidos {dias} {dateAdded}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancela os pedidos com mais de 10 dias no status pendente';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dias = $this->argument('dias');
        $dateAdded = $this->argument('dateAdded');

        $this->line(date('d-m-Y H:i:s') . ' - Selecionando pedidos pendentes com mais de ' . $dias . ' dias');

        $sql = "SELECT 
            o.lv_order_id, 
            o.customer_id,
            l.lv_confirm_picking_id, 
            datediff(NOW(), o.date_added) AS dias,
            sum(op.quantity) as pecas
        FROM morana2.lv_order o 
        JOIN morana2.lv_order_product op ON op.lv_order_id = o.lv_order_id
        LEFT JOIN (
            SELECT cpo.order_id, cpo.lv_confirm_picking_id, cpo.type_system
            FROM lv_confirm_picking_orders cpo 
            JOIN lv_confirm_picking cp ON cp.lv_confirm_picking_id = cpo.lv_confirm_picking_id
            WHERE cp.active = 1
            AND cpo.active = 1
            AND cpo.type_system = 'lv'
        ) AS l ON l.order_id = o.lv_order_id
        WHERE l.lv_confirm_picking_id IS NULL
        AND DATE(o.date_added) >= '" . $dateAdded . "'
        AND o.status_id = 1
        GROUP BY 1,2,3,4
        HAVING dias > " . $dias;

        $pedidosVencidos = DB::select($sql);

        if (count($pedidosVencidos) < 1) {
            $this->line('Nenhum pedido pendente e vencido a mais de ' . $dias . ' dias - '.date('d-m-Y H:i:s'));
            return false;
        }

        foreach ($pedidosVencidos as $pedidoVencido) {
            $this->line(date('d-m-Y H:i:s') . ' - Alterado status do pedido #' . $pedidoVencido->lv_order_id . ' para cancelado');

            $sql = "UPDATE morana2.lv_order 
            SET status_id = '9' 
            WHERE lv_order_id = '" . $pedidoVencido->lv_order_id . "'";

            DB::update($sql);

            $sql = "INSERT INTO morana2.lv_log_pedidos_cancelados SET
            lv_order_id = '" . $pedidoVencido->lv_order_id . "',
            customer_id = '" . $pedidoVencido->customer_id . "',
            days_pendings = '" . $pedidoVencido->dias . "',
            date_added = NOW()";

            DB::insert($sql);
        }
    }
}
