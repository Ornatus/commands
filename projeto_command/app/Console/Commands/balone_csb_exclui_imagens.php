<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class balone_csb_exclui_imagens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:balone-csb-excluir-image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Exclui imagens que estão cadastradas, mas não existem fisicamente';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $sql = "SELECT
      p.product_id,
      p.model,
      p.image
      from balone.cart c
      join morana2.product p on p.product_id = c.product_id
      group by 1,2,3;";

      $products = DB::select($sql);

      foreach ($products as $product) {

      $this->line("Product ID: ".$product->product_id);

        $sql = "SELECT * FROM morana2.product_image
        WHERE  product_id = '".$product->product_id."'
        ORDER BY sort_order";
        $images = DB::select($sql);

        foreach ($images as $image) {
          $file = env('PATH_PPM_IMAGES').$image->image;

          if(!file_exists($file))
          {

            $sql = "DELETE
            FROM product_image
             where product_image_id = '".$image->product_image_id."' ";

             DB::delete($sql);

             $sql = "INSERT INTO  log_exclusao_imagem
             set
             product_id = '".$image->product_id."',
             image = '".$image->image."',
             sort_order = '".$image->sort_order."',
             plataform = 'csb' ";

             DB::insert($sql);
          }

        }

      }



    }
}
