<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use \Datetime;

class morana_csm_email_itens_caixa extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-csm-email-itens-caixa';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Verificar a quantidade de pedidos disponíveis na caixa e envio de email com as informações';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    $this->line('Pegando Ids de todos os customers');

    $sql = "SELECT
    c.customer_id,
    c.brazil_store_name,
    us.email as supervisor_email,
    c.email
    from customer as c
    LEFT JOIN user as us ON us.user_id = c.supervisor_id
    where c.ppe_access = 1
    order by c.customer_id
    ";

    $customers = DB::select($sql);

    foreach ($customers as $customer) {
      $this->line('Coletando pedidos da loja: '.$customer->brazil_store_name);

      $numpedido = 0;

      $data['loja'] = $customer->customer_id;
      $data['brazil_store_name'] = $customer->brazil_store_name;
      $data['supervisor_email'] = $customer->supervisor_email;
      $data['email'] = $customer->email;
      $data['categoria'] = '';
      $data['preco_de'] = '';
      $data['preco_ate'] = '';
      $data['total_preco'] = 0;

      $data['pedidosCsm'] = array();
      $data['pedidosPpm'] = array();
      $data['pedidosLv'] = array();

      $data['data_min_info'] = array();
      $data['data_min'] = '';
      $data['diff_dates'] = 0;
      $data['text_date'] = "";

      $data['info_csm'] = array();
      $info_csm = $this->getInfoCsm($data);
      isset($info_csm[0]->quantidade) ? $data['info_csm']['quantidade'] = $info_csm[0]->quantidade : $data['info_csm']['quantidade']=  0;
      isset($info_csm[0]->preco_total) ? $data['info_csm']['preco_total'] = $info_csm[0]->preco_total : $data['info_csm']['preco_total'] =  0;

      if(isset($info_csm[0]->data_min) && strlen($info_csm[0]->data_min) > 3){
        $data['data_min_info'][] = $info_csm[0]->data_min;
      }

      $data['info_ppm'] = array();
      $info_ppm = $this->getInfoPpm($data);
      isset($info_ppm[0]->quantidade) ? $data['info_ppm']['quantidade'] = $info_ppm[0]->quantidade : $data['info_ppm']['quantidade'] = 0;
      isset($info_ppm[0]->preco_total) ? $data['info_ppm']['preco_total'] = $info_ppm[0]->preco_total : $data['info_ppm']['preco_total'] = 0;

      if(isset($info_ppm[0]->data_min) && strlen($info_ppm[0]->data_min) > 3){
        $data['data_min_info'][] = $info_ppm[0]->data_min;
      }

      $data['info_lv'] = array();
      $info_lv = $this->getInfoLv($data);
      isset($info_lv[0]->quantidade) ? $data['info_lv']['quantidade'] = $info_lv[0]->quantidade : $data['info_lv']['quantidade'] = 0;
      isset($info_lv[0]->preco_total) ? $data['info_lv']['preco_total'] = $info_lv[0]->preco_total : $data['info_lv']['preco_total'] = 0;

      if(isset($info_lv[0]->data_min) && strlen($info_lv[0]->data_min) > 3){
        $data['data_min_info'][] = $info_lv[0]->data_min;
      }

      $data['total_products'] = $data['info_csm']['quantidade'] + $data['info_ppm']['quantidade']+ $data['info_lv']['quantidade'];
      $data['total_price'] = $data['info_csm']['preco_total'] + $data['info_ppm']['preco_total']+ $data['info_lv']['preco_total'];

      $data['data_min'] = MIN($data['data_min_info']);

      $datetime1 = new DateTime($data['data_min']);
      $datetime2 = new DateTime(date("Y-m-d"));
      $interval = $datetime1->diff($datetime2);
      $data['diff_dates'] = $interval->days;

      if($data['diff_dates'] > 31){
        $data['text_date'] = "(".floor($data['diff_dates'] / 31)." MESES)";
      }else{
        $data['text_date'] = "(".$data['diff_dates']." DIAS)";
      }

      $valorTotalCsm = 0;
      $valorTotalPpm = 0;
      $valorTotalLojavirtual = 0;

      $precoTotalCsm = 0;
      $precoTotalPpm = 0;
      $precoTotalLv = 0;

      $data['colunas'] = array('BRINCO','COLAR','PULSEIRA','ANEL','CONJUNTO','TORNOZELEIRA','PINGENTE','OUTROS');

      $this->line('Coletando pedidos CSM');
      $pedidosCsm = $this->getPedidosCsm($data);
      $this->line('Coletando pedidos PPM');
      $pedidosPpm = $this->getPedidosPpm($data);
      $this->line('Coletando pedidos LOJA VIRTUAL');
      $pedidosLv = $this->getPedidosLojavirtual($data);

      $pedido_csm = 0;

      foreach($pedidosCsm as $pedido)
      {
        if($pedido_csm != $pedido->pedido)
        {
          $pedido_csm = $pedido->pedido;
          $valorTotalCsm = 0;
          $precoTotalCsm = 0;
        }
        $data['pedidosCsm'][$pedido->pedido][$pedido->categoria] = $pedido;
        $data['pedidosCsm'][$pedido->pedido]['data'] = $pedido->data;
        $data['pedidosCsm'][$pedido->pedido]['loja'] = $pedido->customer_id;

        $valorTotalCsm +=$pedido->quantidade;
        $data['pedidosCsm'][$pedido->pedido]['total'] = $valorTotalCsm;

        $precoTotalCsm +=$pedido->preco_total;
        $data['pedidosCsm'][$pedido->pedido]['preco_total'] = $precoTotalCsm;

      }

      $pedido_ppm = 0;
      foreach($pedidosPpm as $pedido)
      {
        if($pedido_ppm != $pedido->pedido)
        {
          $pedido_ppm = $pedido->pedido;
          $valorTotalPpm = 0;
          $precoTotalPpm = 0;
        }

        $data['pedidosPpm'][$pedido->pedido][$pedido->categoria] = $pedido;
        $data['pedidosPpm'][$pedido->pedido]['data'] = $pedido->data;
        $data['pedidosPpm'][$pedido->pedido]['loja'] = $pedido->customer_id;

        $valorTotalPpm +=$pedido->quantidade;
        $data['pedidosPpm'][$pedido->pedido]['total'] = $valorTotalPpm;

        $precoTotalPpm +=$pedido->preco_total;
        $data['pedidosPpm'][$pedido->pedido]['preco_total'] = $precoTotalPpm;

      }

      $pedido_lv = 0;
      foreach($pedidosLv as $pedido)
      {

        if($pedido_lv != $pedido->pedido)
        {
          $pedido_lv = $pedido->pedido;
          $valorTotalLojavirtual = 0;
          $precoTotalLv = 0;
        }

        $data['pedidosLv'][$pedido->pedido][$pedido->categoria] = $pedido;
        $data['pedidosLv'][$pedido->pedido]['data'] = $pedido->data;
        $data['pedidosLv'][$pedido->pedido]['loja'] = $pedido->customer_id;

        $valorTotalLojavirtual +=$pedido->quantidade;
        $data['pedidosLv'][$pedido->pedido]['total'] = $valorTotalLojavirtual;

        $precoTotalLv +=$pedido->preco_total;
        $data['pedidosLv'][$pedido->pedido]['preco_total'] = $precoTotalLv;
      }


      $this->line('Pegando emails do Franqueado:'.$customer->brazil_store_name);

      $sql = "SELECT
      *
      FROM customer_email
      WHERE customer_id = '".$customer->customer_id."'";

      $info_email = DB::select($sql);

      $data['info_email'] = $info_email;

      $this->line('Montando e enviando email');

      if(count($pedidosLv > 0) || count($pedidosPpm > 0) || count($pedidosCsm > 0)){
        $this->mailFranqueado($data);
      }else{
        $this->line('Não tem pedidos na caixa...');
      }


      $this->line('Próximo franqueado...');


    }


  }




  function getPedidosCsm($data)
  {
    $sql = "SELECT
    'CSM' as plataforma,
    o.customer_id,
    (select MAX(date(date_added)) from order_status_log where order_id = o.order_id and order_status_id = 3 ) as data,
    o.order_id as pedido,
    CASE WHEN substring(p.model,8,3) NOT IN ('ERR','BRC','NCL','RNG','SET','ANK','PDT') THEN 'OUTROS'
    ELSE
    substring(p.model,8,3) END as categoria,
    CASE WHEN o.purchase_type = 'R' THEN
    coalesce(sum(op.quantity_partial),0)
    ELSE
    coalesce(sum(op.quantity),0) + coalesce(sum(op.extra_quantity),0)
    END AS quantidade,
    CASE
    WHEN o.purchase_type = 'R' THEN ( round(COALESCE(SUM(op.quantity_partial), 0)*fp.fixed_price,2))
    ELSE
    (
      COALESCE(
        SUM( op.quantity*(fp.fixed_price + round((fp.suggested_price/100)*5,2) )), 0
      ) +
      COALESCE(
        SUM(op.extra_quantity*(fp.fixed_price+ round((fp.suggested_price/100)*5,2))), 0)
      )
      END AS preco_total
      FROM webstore.order o
      JOIN webstore.order_product op ON op.order_id = o.order_id
      JOIN morana2.product p ON p.product_id = op.product_id
      JOIN morana2.product_fixed_price_brazil fp ON fp.product_id = p.product_id
      JOIN morana2.customer c ON c.customer_id = o.customer_id
      WHERE o.order_status_id = 3
      AND o.customer_id = '".$data['loja']."'
      AND o.order_id not in (
        SELECT lpo.order_id from lv_confirm_picking lp
        JOIN lv_confirm_picking_orders lpo ON lpo.lv_confirm_picking_id = lp.lv_confirm_picking_id
        WHERE lp.active = 1
        AND lpo.active = 1
        AND lp.customer_id = '".$data['loja']."'
        AND lpo.type_system = 'csm')
        GROUP BY 1,2,3,4,5
        HAVING quantidade > 0
        ORDER BY o.order_id ";

        return (array)DB::select($sql);
      }


      function getPedidosPpm($data)
      {
        $sql = "SELECT
        'PPM' as plataforma,
        o.customer_id,
        DATE(min(op.date_picked)) as data,
        op.week_number as pedido,
        CASE WHEN substring(p.model,8,3) NOT IN ('ERR','BRC','NCL','RNG','SET','ANK','PDT') THEN 'OUTROS'
        ELSE
        substring(p.model,8,3) END as categoria,
        coalesce(sum(op.quantity),0)  AS quantidade,
        ( COALESCE(
          SUM(op.quantity*(fp.fixed_price+ round((fp.suggested_price/100)*5,2))), 0)
        )  AS preco_total
        FROM morana2.order o
        JOIN morana2.order_product op ON op.order_id = o.order_id
        JOIN morana2.product p ON p.product_id = op.product_id
        JOIN morana2.product_fixed_price_brazil fp ON fp.product_id = p.product_id
        JOIN morana2.customer c ON c.customer_id = o.customer_id
        WHERE op.order_product_status_id = 8
        AND o.customer_id = '".$data['loja']."'
        GROUP BY 1,2,4,5
        ORDER BY op.week_number , 3 desc ";

        return (array)DB::select($sql);
      }


      static function getPedidosLojavirtual($data)
      {
        $sql = "SELECT
        'LOJAVIRTUAL' as plataforma,
        o.customer_id,
        DATE(max(o.date_box)) as data,
        op.lv_order_id as pedido,
        CASE WHEN substring(p.model,8,3) NOT IN ('ERR','BRC','NCL','RNG','SET','ANK','PDT') THEN 'OUTROS'
        ELSE
        substring(p.model,8,3) END as categoria,
        coalesce(sum(op.quantity_partial),0)  AS quantidade,
        ( COALESCE(
          SUM(op.quantity_partial*(fp.fixed_price+ round((fp.suggested_price/100)*5,2))), 0)
        )  AS preco_total
        FROM morana2.lv_order o
        JOIN morana2.lv_order_product op ON op.lv_order_id = o.lv_order_id
        JOIN morana2.product p ON p.product_id = op.product_id
        JOIN morana2.product_fixed_price_brazil fp ON fp.product_id = p.product_id
        JOIN morana2.customer c ON c.customer_id = o.customer_id
        WHERE o.status_id = 3
        AND o.customer_id = '".$data['loja']."'
        AND op.quantity_partial > 0
        AND o.lv_order_id not in (
          SELECT lpo.order_id from lv_confirm_picking lp
          JOIN lv_confirm_picking_orders lpo ON lpo.lv_confirm_picking_id = lp.lv_confirm_picking_id
          WHERE lp.active = 1
          AND lpo.active = 1
          AND lp.customer_id = '".$data['loja']."'
          AND lpo.type_system = 'lv')
          GROUP BY 1,2,4,5
          ORDER BY o.lv_order_id ";

          return (array)DB::select($sql);
        }


        function getPpmFiltro($data)
        {
          $sql = "SELECT
          distinct op.week_number as pedido
          FROM morana2.order o
          JOIN morana2.order_product op ON op.order_id = o.order_id
          JOIN morana2.product p ON p.product_id = op.product_id
          JOIN morana2.product_fixed_price_brazil fp ON fp.product_id = p.product_id
          JOIN morana2.customer c ON c.customer_id = o.customer_id
          WHERE op.order_product_status_id = 8
          AND o.customer_id = '".$data['loja']."'" ;

          $semanas = (array)DB::select($sql);

          $in = '';

          foreach($semanas as $semana)
          {
            $in .= '\''.$semana->pedido.'\',';
          }
          $in .='\'0\'';

          return $in;

        }


        public function mailFranqueado($data)
        {

          $view = 'mail/lista_pedidos';

          $data['emails'] = array();

          if(isset($data['info_email'][0]->email1) && $data['info_email'][0]->email1 != ''){
            $data['emails'][] = $data['info_email'][0]->email1;
          }

          if(isset($data['info_email'][0]->email2) && $data['info_email'][0]->email2 != ''){
            $data['emails'][] = $data['info_email'][0]->email2;
          }

          if(isset($data['info_email'][0]->email3) && $data['info_email'][0]->email3 != ''){
            $data['emails'][] = $data['info_email'][0]->email3;
          }

          if(isset($data['supervisor_email']) && $data['supervisor_email'] != ''){
            $data['emails'][] = $data['supervisor_email'];
          }

          if(isset($data['email']) && $data['email'] != ''){
            $data['emails'][] = $data['email'];
          }

          //$data['emails'] = array();
          //$data['emails'][] = 'caioymt@gmail.com';
          $data['emails'][] = 'fabio.liossi@gmail.com';

          if(count($data['emails']) > 0){
            Mail::send($view, $data, function($message) use ($data) {
              $message->to($data['emails'],'')->subject( '[MORANA] Produtos na Caixa - LOJA: '.$data['brazil_store_name'] );
              $message->from('dev@grupoornatus.com','[MORANA]');
            });
          }else{
            return true;
          }
        }


        function getInfoCsm($data)
        {
          $sql = "SELECT
          SUM(quantidade) as quantidade,
          SUM(preco_total) as preco_total,
          MIN(data) as data_min
          FROM
          (SELECT
            'CSM' as plataforma,
            o.customer_id,
            (select MAX(date(date_added)) from order_status_log where order_id = o.order_id and order_status_id = 3 ) as data,
            o.order_id as pedido,
            CASE WHEN substring(p.model,8,3) NOT IN ('ERR','BRC','NCL','RNG','SET','ANK','PDT') THEN 'OUTROS'
            ELSE
            substring(p.model,8,3) END as categoria,
            CASE WHEN o.purchase_type = 'R' THEN
            coalesce(sum(op.quantity_partial),0)
            ELSE
            coalesce(sum(op.quantity),0) + coalesce(sum(op.extra_quantity),0)
            END AS quantidade,
            CASE
            WHEN o.purchase_type = 'R' THEN ( round(COALESCE(SUM(op.quantity_partial), 0)*fp.fixed_price,2))
            ELSE
            (
              COALESCE(
                SUM( op.quantity*(fp.fixed_price + round((fp.suggested_price/100)*5,2) )), 0
              ) +
              COALESCE(
                SUM(op.extra_quantity*(fp.fixed_price+ round((fp.suggested_price/100)*5,2))), 0)
              )
              END AS preco_total
              FROM webstore.order o
              JOIN webstore.order_product op ON op.order_id = o.order_id
              JOIN morana2.product p ON p.product_id = op.product_id
              JOIN morana2.product_fixed_price_brazil fp ON fp.product_id = p.product_id
              JOIN morana2.customer c ON c.customer_id = o.customer_id
              WHERE o.order_status_id = 3
              AND o.customer_id = '".$data['loja']."'
              AND o.order_id not in (
                SELECT lpo.order_id from lv_confirm_picking lp
                JOIN lv_confirm_picking_orders lpo ON lpo.lv_confirm_picking_id = lp.lv_confirm_picking_id
                WHERE lp.active = 1
                AND lpo.active = 1
                AND lp.customer_id = '".$data['loja']."'
                AND lpo.type_system = 'csm')
                GROUP BY 1,2,3,4,5
                HAVING quantidade > 0
                ORDER BY o.order_id ) as p";



                return (array)DB::select($sql);
              }


              function getInfoPpm($data)
              {
                $sql = "SELECT
                SUM(quantidade) as quantidade,
                SUM(preco_total) as preco_total,
                MIN(data) as data_min
                FROM
                (SELECT
                  'PPM' as plataforma,
                  o.customer_id,
                  DATE(max(op.date_picked)) as data,
                  op.week_number as pedido,
                  CASE WHEN substring(p.model,8,3) NOT IN ('ERR','BRC','NCL','RNG','SET','ANK','PDT') THEN 'OUTROS'
                  ELSE
                  substring(p.model,8,3) END as categoria,
                  coalesce(sum(op.quantity),0)  AS quantidade,
                  ( COALESCE(
                    SUM(op.quantity*(fp.fixed_price+ round((fp.suggested_price/100)*5,2))), 0)
                  )  AS preco_total
                  FROM morana2.order o
                  JOIN morana2.order_product op ON op.order_id = o.order_id
                  JOIN morana2.product p ON p.product_id = op.product_id
                  JOIN morana2.product_fixed_price_brazil fp ON fp.product_id = p.product_id
                  JOIN morana2.customer c ON c.customer_id = o.customer_id
                  WHERE op.order_product_status_id = 8
                  AND o.customer_id = '".$data['loja']."' GROUP BY 1,2,4,5
                  ORDER BY op.week_number ) as p";

                  return (array)DB::select($sql);
                }

                static function getInfoLv($data)
                {
                  $sql = "SELECT
                  SUM(quantidade) as quantidade,
                  SUM(preco_total) as preco_total,
                  MIN(data) as data_min
                  FROM
                  (SELECT
                    'LOJAVIRTUAL' as plataforma,
                    o.customer_id,
                    DATE(max(o.date_box)) as data,
                    op.lv_order_id as pedido,
                    CASE WHEN substring(p.model,8,3) NOT IN ('ERR','BRC','NCL','RNG','SET','ANK','PDT') THEN 'OUTROS'
                    ELSE
                    substring(p.model,8,3) END as categoria,
                    coalesce(sum(op.quantity_partial),0)  AS quantidade,
                    ( COALESCE(
                      SUM(op.quantity_partial*(fp.fixed_price+ round((fp.suggested_price/100)*5,2))), 0)
                    )  AS preco_total
                    FROM morana2.lv_order o
                    JOIN morana2.lv_order_product op ON op.lv_order_id = o.lv_order_id
                    JOIN morana2.product p ON p.product_id = op.product_id
                    JOIN morana2.product_fixed_price_brazil fp ON fp.product_id = p.product_id
                    JOIN morana2.customer c ON c.customer_id = o.customer_id AND type_user_id = 1
                    WHERE o.status_id = 3
                    AND o.customer_id = '".$data['loja']."'
                    AND op.quantity_partial > 0
                    AND o.lv_order_id not in (
                      SELECT lpo.order_id from lv_confirm_picking lp
                      JOIN lv_confirm_picking_orders lpo ON lpo.lv_confirm_picking_id = lp.lv_confirm_picking_id
                      WHERE lp.active = 1
                      AND lpo.active = 1
                      AND lp.customer_id = '".$data['loja']."'
                      AND lpo.type_system = 'lv')
                      GROUP BY 1,2,4,5
                      ORDER BY o.lv_order_id ) as p";


                      return (array)DB::select($sql);
                    }

                  }
