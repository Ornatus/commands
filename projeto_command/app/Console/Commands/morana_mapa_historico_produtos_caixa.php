<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;




class morana_mapa_historico_produtos_caixa extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-mapa-historico-produtos-caixa';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Command description';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {

    ini_set('memory_limit','1024M'); // set memory to prevent fatal errors

    date_default_timezone_set('America/Sao_Paulo');

    $this->line('Coletando dados PPM '.date('d-m-Y H:i:s'));

    $sql = "SELECT
    l.lv_confirm_picking_id as liberacao,
    b.*
    FROM vw_report_product_box_ppm b
    LEFT JOIN(
      SELECT
      lv.customer_id,
      lvo.week_number,
      max(lvo.lv_confirm_picking_id) as  lv_confirm_picking_id
      FROM
      lv_confirm_picking_orders lvo
      join lv_confirm_picking lv on lv.lv_confirm_picking_id = lvo.lv_confirm_picking_id
      where lvo.type_system = 'ppm'
      and lv.active = 1
      and lv.is_admin = 0
      and lvo.active = 1
      and lv.tipo_usuario = 1
      group by 1,2) l on l.week_number = b.semana AND b.customer_id = l.customer_id";

      $results_ppm =  DB::select($sql);

      $this->line('Terminou query do PPM '.date('d-m-Y H:i:s'));

      $pages=0;
      $pages = ceil(count($results_ppm) / 50);
      $page_init = 0;
      $inserts = 0;

      $sql_insert = "INSERT INTO mapa.morana_historico_produtos_caixa
      (
        plataforma,
        date,
        pedido,
        area,
        loja,
        codigo,
        modelo,
        categoria,
        preco,
        semana,
        date_box,
        quantidade,
        liberacao,
        customer_id,
        product_id
      )
      VALUES";

      $sql = "";

      foreach ($results_ppm as $result) {

        //$sql = "";

        $sql.= "(
          '".$result->plataforma."',
          '".$result->data."',
          '".$result->pedido."',
          '".$result->area."',
          '".$result->loja."',
          '".$result->brazil_code."',
          '".$result->modelo."',
          '".$result->categoria."',
          '".$result->preco."',
          '".$result->semana."',
          '".$result->date_box."',
          '".$result->quantidade."',
          '".$result->liberacao."',
          '".$result->customer_id."',
          '".$result->product_id."'),";

          $inserts++;

          if($inserts == 50){
            $sql = substr($sql, 0, -1);
            DB::insert($sql_insert.$sql);
            $sql = "";
            $inserts = 0;
            $page_init++;


          }elseif($page_init == ($pages -1)){
            $sql = substr($sql, 0, -1);
            DB::insert($sql_insert.$sql);
            $sql = "";
          }


        }

        $this->line('Inseriu PPM '.date('d-m-Y H:i:s'));



        $sql = "SELECT
        b.*,
        (SELECT max(lvo.lv_confirm_picking_id) lv_confirm_picking_id
        FROM
        lv_confirm_picking_orders lvo
        join lv_confirm_picking lv on lv.lv_confirm_picking_id = lvo.lv_confirm_picking_id
        where lvo.type_system = 'csm'
        and lv.active = 1
        and lv.is_admin = 0
        and lvo.active = 1
        and lvo.order_id = b.pedido limit 1) as liberacao
        FROM (SELECT
          'CSM' AS plataforma,
          o.date_added AS data,
          o.order_id AS pedido,
          c.customer_id AS customer_id,
          c.brazil_store_id AS area,
          c.brazil_store_name AS loja,
          p.product_id AS product_id,
          p.brazil_code AS brazil_code,
          p.model AS modelo,
          SUBSTR(p.model, 8, 3) AS categoria,
          fp.suggested_price AS preco,
          o.news_week_id AS semana,
          dt_box.date_box AS date_box,
          (CASE
            WHEN (o.purchase_type <> 'R') THEN (COALESCE(op.quantity, 0) + COALESCE(op.extra_quantity, 0))
            ELSE COALESCE(op.quantity_partial, 0)
            END) AS quantidade
            FROM
            (((((webstore.order_product op
              JOIN webstore.order o ON ((o.order_id = op.order_id)))
              JOIN morana2.customer c ON ((c.customer_id = o.customer_id)))
              JOIN morana2.product p ON ((p.product_id = op.product_id)))
              JOIN morana2.product_fixed_price_brazil fp ON ((fp.product_id = p.product_id)))
              LEFT JOIN (SELECT
                morana2.order_status_log.order_id AS order_id,
                CAST(MAX(morana2.order_status_log.date_added)
                AS DATE) AS date_box
                FROM
                morana2.order_status_log
                WHERE
                (morana2.order_status_log.order_status_id = 3)
                GROUP BY morana2.order_status_log.order_id) dt_box ON ((dt_box.order_id = o.order_id)))
                WHERE
                ((o.order_status_id = 3)
                AND ((op.quantity > 0)
                OR (op.extra_quantity > 0)))
              ) b";

              $results_csm =  DB::select($sql);

              $this->line('Terminou query do CSM '.date('d-m-Y H:i:s'));

              $pages=0;
              $pages = ceil(count($results_csm) / 50);
              $page_init = 0;
              $inserts = 0;

              $sql_insert = "INSERT INTO mapa.morana_historico_produtos_caixa
              (
                plataforma,
                date,
                pedido,
                area,
                loja,
                codigo,
                modelo,
                categoria,
                preco,
                semana,
                date_box,
                quantidade,
                liberacao,
                customer_id,
                product_id
              )
              VALUES";

              $sql = "";

              foreach ($results_csm as $result) {

                //$sql = "";

                $sql.= "(
                  '".$result->plataforma."',
                  '".$result->data."',
                  '".$result->pedido."',
                  '".$result->area."',
                  '".$result->loja."',
                  '".$result->brazil_code."',
                  '".$result->modelo."',
                  '".$result->categoria."',
                  '".$result->preco."',
                  '".$result->semana."',
                  '".$result->date_box."',
                  '".$result->quantidade."',
                  '".$result->liberacao."',
                  '".$result->customer_id."',
                  '".$result->product_id."'),";

                  $inserts++;

                  if($inserts == 50){
                    $sql = substr($sql, 0, -1);
                    DB::insert($sql_insert.$sql);
                    $sql = "";
                    $inserts = 0;
                    $page_init++;


                  }elseif($page_init == ($pages -1)){
                    $sql = substr($sql, 0, -1);
                    DB::insert($sql_insert.$sql);
                    $sql = "";
                  }


                }


                $this->line('Inseriu CSM '.date('d-m-Y H:i:s'));


                $sql = "SELECT
                b.*,
                (SELECT max(lvo.lv_confirm_picking_id) lv_confirm_picking_id
                FROM
                lv_confirm_picking_orders lvo
                join lv_confirm_picking lv on lv.lv_confirm_picking_id = lvo.lv_confirm_picking_id
                where lvo.type_system = 'lv'
                and lv.active = 1
                and lv.is_admin = 0
                and lvo.active = 1
                and lvo.order_id = b.pedido limit 1) as liberacao
                FROM vw_report_product_box_lojavirtual b;";

                $results_lojavirtual =  DB::select($sql);

                $this->line('Terminou query do LV '.date('d-m-Y H:i:s'));

                $pages= 0;
                $pages = ceil(count($results_lojavirtual) / 50);
                $page_init = 0;
                $inserts = 0;

                $sql_insert = "INSERT INTO mapa.morana_historico_produtos_caixa
                (
                  plataforma,
                  date,
                  pedido,
                  area,
                  loja,
                  codigo,
                  modelo,
                  categoria,
                  preco,
                  semana,
                  date_box,
                  quantidade,
                  liberacao,
                  customer_id,
                  product_id
                )
                VALUES";

                $sql = "";

                foreach ($results_lojavirtual as $result) {

                  //$sql = "";

                  $sql.= "(
                    '".$result->plataforma."',
                    '".$result->data."',
                    '".$result->pedido."',
                    '".$result->area."',
                    '".$result->loja."',
                    '".$result->brazil_code."',
                    '".$result->modelo."',
                    '".$result->categoria."',
                    '".$result->preco."',
                    '".$result->semana."',
                    '".$result->date_box."',
                    '".$result->quantidade."',
                    '".$result->liberacao."',
                    '".$result->customer_id."',
                    '".$result->product_id."'),";

                    $inserts++;

                    if($inserts == 50){
                      $sql = substr($sql, 0, -1);
                      DB::insert($sql_insert.$sql);
                      $sql = "";
                      $inserts = 0;
                      $page_init++;


                    }elseif($page_init == ($pages -1)){
                      $sql = substr($sql, 0, -1);
                      DB::insert($sql_insert.$sql);
                      $sql = "";
                    }


                  }

                  $this->line('Inseriu LV '.date('d-m-Y H:i:s'));

                }
              }
