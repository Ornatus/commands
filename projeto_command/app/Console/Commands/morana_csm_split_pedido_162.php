<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class morana_csm_split_pedido_162 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:morana-csm-split-pedido-162';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Faz o split dos pedida da semana 163';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->line('Pegando os pedidos da semana 163');

      $sql   = "SELECT * FROM webstore.order o
                JOIN customer c ON c.customer_id = o.customer_id
                where o.news_week_id = 163
                AND o.purchase_type in('N','A')
                AND o.order_status_id = 3
                AND (o.firstname not like '%óxima%')
                AND (o.firstname not like '%MKT%')
                AND o.order_id not in
                (
                select order_id from lv_confirm_picking_orders where type_system = 'csm' and active = 1
              )
              AND c.brazil_store_id in
              (
                '001',
'002',
'006',
'007',
'014',
'017',
'026',
'029',
'033',
'047',
'052',
'055',
'112',
'145',
'177',
'178',
'208',
'210',
'213',
'220',
'232',
'236',
'238',
'247',
'285',
'294',
'354',
'363',
'412',
'413',
'442',
'466',
'482',
'491',
'492',
'508',
'540',
'549',
'564',
'615',
'619',
'626',
'630',
'653',
'657',
'659',
'670',
'678',
'688',
'699',
'711',
'716',
'728',
'732',
'734',
'735',
'742',
'745',
'753',
'088',
'571',
'576',
'594',
'620'


              )  order by c.customer_id;";

      $orders = DB::select($sql);

      $this->line('Total de pedidos: '.count($orders) );

      foreach($orders as $order)
      {
        $pedido = $order->order_id;

        $this->line('LOJA ['.$order->firstname.'] pedido ['.$pedido.']' );

        $sql = "INSERT INTO `webstore`.`order`
                (
                `invoice_no`,
                `invoice_prefix`,
                `store_id`,
                `store_name`,
                `store_url`,
                `customer_id`,
                `customer_group_id`,
                `firstname`,
                `lastname`,
                `email`,
                `telephone`,
                `fax`,
                `custom_field`,
                `payment_firstname`,
                `payment_lastname`,
                `payment_company`,
                `payment_address_1`,
                `payment_address_2`,
                `payment_city`,
                `payment_postcode`,
                `payment_country`,
                `payment_country_id`,
                `payment_zone`,
                `payment_zone_id`,
                `payment_address_format`,
                `payment_custom_field`,
                `payment_method`,
                `payment_code`,
                `shipping_firstname`,
                `shipping_lastname`,
                `shipping_company`,
                `shipping_address_1`,
                `shipping_address_2`,
                `shipping_city`,
                `shipping_postcode`,
                `shipping_country`,
                `shipping_country_id`,
                `shipping_zone`,
                `shipping_zone_id`,
                `shipping_address_format`,
                `shipping_custom_field`,
                `shipping_method`,
                `shipping_code`,
                `comment`,
                `total`,
                `order_status_id`,
                `affiliate_id`,
                `commission`,
                `marketing_id`,
                `tracking`,
                `language_id`,
                `currency_id`,
                `currency_code`,
                `currency_value`,
                `ip`,
                `forwarded_ip`,
                `user_agent`,
                `accept_language`,
                `date_added`,
                `date_modified`,
                `news_week_id`,
                `purchase_type`,
                `repproved_comment`,
                `is_save_cart`,
                `datetime_invoice`)

                SELECT
                    1 as invoice_no,
                    `order`.`invoice_prefix`,
                    `order`.`store_id`,
                    `order`.`store_name`,
                    `order`.`store_url`,
                    `order`.`customer_id`,
                    `order`.`customer_group_id`,
                    `order`.`firstname`,
                    `order`.`lastname`,
                    `order`.`email`,
                    `order`.`telephone`,
                    `order`.`fax`,
                    `order`.`custom_field`,
                    `order`.`payment_firstname`,
                    `order`.`payment_lastname`,
                    `order`.`payment_company`,
                    `order`.`payment_address_1`,
                    `order`.`payment_address_2`,
                    `order`.`payment_city`,
                    `order`.`payment_postcode`,
                    `order`.`payment_country`,
                    `order`.`payment_country_id`,
                    `order`.`payment_zone`,
                    `order`.`payment_zone_id`,
                    `order`.`payment_address_format`,
                    `order`.`payment_custom_field`,
                    `order`.`payment_method`,
                    `order`.`payment_code`,
                    `order`.`shipping_firstname`,
                    `order`.`shipping_lastname`,
                    `order`.`shipping_company`,
                    `order`.`shipping_address_1`,
                    `order`.`shipping_address_2`,
                    `order`.`shipping_city`,
                    `order`.`shipping_postcode`,
                    `order`.`shipping_country`,
                    `order`.`shipping_country_id`,
                    `order`.`shipping_zone`,
                    `order`.`shipping_zone_id`,
                    `order`.`shipping_address_format`,
                    `order`.`shipping_custom_field`,
                    `order`.`shipping_method`,
                    `order`.`shipping_code`,
                    `order`.`comment`,
                    `order`.`total`,
                    2 as order_status_id,
                    `order`.`affiliate_id`,
                    `order`.`commission`,
                    `order`.`marketing_id`,
                    `order`.`tracking`,
                    `order`.`language_id`,
                    `order`.`currency_id`,
                    `order`.`currency_code`,
                    `order`.`currency_value`,
                    `order`.`ip`,
                    `order`.`forwarded_ip`,
                    `order`.`user_agent`,
                    `order`.`accept_language`,
                    `order`.`date_added`,
                    `order`.`date_modified`,
                    `order`.`news_week_id`,
                    `order`.`purchase_type`,
                    `order`.`repproved_comment`,
                    `order`.`is_save_cart`,
                    `order`.`datetime_invoice`
                FROM `webstore`.`order`
                where order_id = '".$pedido."' and news_week_id = 163;";

        DB::insert($sql);

        $pedido_1 = DB::getPdo()->lastInsertId();

        if((int)$pedido_1<1)
        {
            $this->line('ERRO AO CRIAR O PEDIDO 1');

            return false;
        }

        $this->line('Pedido['.$pedido_1.'] gerado com sucesso!');

        $update1 = "UPDATE webstore.order_product op
                    join product p on p.product_id = op.product_id
                    SET op.order_id = '".$pedido_1."'
                    WHERE op.order_id = '".$pedido."'
                    AND p.model IN (
                      '13-149-NCL-03169-GD-SA',
                      '13-149-NCL-03169-GD-TL',
                      '13-149-NCL-03169-GD-CR',
                      '13-149-NCL-03169-BK-SA',
                      '13-149-NCL-03169-BK-CR',
                      '13-149-NCL-03169-BK-TL',
                      '13-053-ERR-00765-GD',
                      '13-053-ERR-00765-OR',
                      '13-418-ERR-00692-GD-60',
                      '13-418-ERR-00692-OR-60',
                      '13-418-ERR-00692-BK-60',
                      '13-420-RNG-00077-GD-CR-00',
                      '13-420-RNG-00077-OR-CR-00',
                      '13-116-BRC-00209-GD-CR',
                      '13-116-BRC-00209-OR-CR',
                      '13-149-BRC-03148-GD-CR',
                      '13-149-BRC-03148-OR-CR',
                      '13-053-BRC-00814-GD-CR',
                      '13-053-BRC-00814-OR-CR',
                      '13-224-NCL-00077-GD',
                      '13-224-NCL-00077-OR',
                      '13-224-NCL-00077-BK',
                      '13-224-NCL-00078-GD',
                      '13-224-NCL-00078-OR',
                      '13-224-NCL-00078-BK',
                      '13-418-ERR-01214-GD',
                      '13-418-ERR-01214-OR',
                      '13-414-ERR-00378-GD-CR',
                      '13-414-ERR-00378-OR-CR',
                      '13-153-ERR-00122-GD-CR',
                      '13-153-ERR-00122-OR-CR',
                      '13-224-BRC-00079-GD',
                      '13-224-BRC-00079-OR',
                      '13-002-BRC-01548-GD',
                      '13-002-BRC-01548-OR',
                      '13-501-NCL-00137-GD-CR',
                      '13-501-NCL-00137-OR-CR',
                      '13-149-NCL-03340-GD-CR',
                      '13-149-NCL-03340-OR-CR',
                      '13-149-ERR-03239-GD',
                      '13-149-ERR-03239-OR',
                      '13-149-ERR-03239-BK',
                      '13-414-ERR-00379-GD-CR',
                      '13-414-ERR-00379-OR-CR',
                      '13-152-ERR-00155-GD-CR',
                      '13-152-ERR-00155-OR-CR',
                      '13-420-RNG-00068-GD-CR-00',
                      '13-420-RNG-00068-OR-CR-00',
                      '13-420-RNG-00068-BK-CR-00',
                      '13-053-BRC-00827-GD',
                      '13-053-BRC-00827-OR',
                      '13-501-NCL-00217-GD-CR',
                      '13-501-NCL-00217-OR-CR',
                      '13-501-NCL-00176-GD-CR',
                      '13-501-NCL-00176-OR-CR',
                      '13-411-ERR-01437-GD-CR',
                      '13-411-ERR-01437-OR-CR',
                      '13-411-ERR-01437-BK-CR',
                      '13-195-ERR-00467-GD-CR',
                      '13-195-ERR-00467-OR-CR',
                      '13-077-BRC-00215-GD-CR',
                      '13-077-BRC-00243-GD-CR',
                      '13-077-BRC-00233-GD-CR',
                      '13-077-BRC-00235-GD-CR',
                      '13-077-BRC-00238-GD-CR'
                    );";

                    DB::insert($update1);

                  $this->line('Atualizado o  Pedido['.$pedido_1.']');

                  $sql = "INSERT INTO `webstore`.`order`
                          (
                          `invoice_no`,
                          `invoice_prefix`,
                          `store_id`,
                          `store_name`,
                          `store_url`,
                          `customer_id`,
                          `customer_group_id`,
                          `firstname`,
                          `lastname`,
                          `email`,
                          `telephone`,
                          `fax`,
                          `custom_field`,
                          `payment_firstname`,
                          `payment_lastname`,
                          `payment_company`,
                          `payment_address_1`,
                          `payment_address_2`,
                          `payment_city`,
                          `payment_postcode`,
                          `payment_country`,
                          `payment_country_id`,
                          `payment_zone`,
                          `payment_zone_id`,
                          `payment_address_format`,
                          `payment_custom_field`,
                          `payment_method`,
                          `payment_code`,
                          `shipping_firstname`,
                          `shipping_lastname`,
                          `shipping_company`,
                          `shipping_address_1`,
                          `shipping_address_2`,
                          `shipping_city`,
                          `shipping_postcode`,
                          `shipping_country`,
                          `shipping_country_id`,
                          `shipping_zone`,
                          `shipping_zone_id`,
                          `shipping_address_format`,
                          `shipping_custom_field`,
                          `shipping_method`,
                          `shipping_code`,
                          `comment`,
                          `total`,
                          `order_status_id`,
                          `affiliate_id`,
                          `commission`,
                          `marketing_id`,
                          `tracking`,
                          `language_id`,
                          `currency_id`,
                          `currency_code`,
                          `currency_value`,
                          `ip`,
                          `forwarded_ip`,
                          `user_agent`,
                          `accept_language`,
                          `date_added`,
                          `date_modified`,
                          `news_week_id`,
                          `purchase_type`,
                          `repproved_comment`,
                          `is_save_cart`,
                          `datetime_invoice`)

                          SELECT
                              2 as invoice_no,
                              `order`.`invoice_prefix`,
                              `order`.`store_id`,
                              `order`.`store_name`,
                              `order`.`store_url`,
                              `order`.`customer_id`,
                              `order`.`customer_group_id`,
                              `order`.`firstname`,
                              `order`.`lastname`,
                              `order`.`email`,
                              `order`.`telephone`,
                              `order`.`fax`,
                              `order`.`custom_field`,
                              `order`.`payment_firstname`,
                              `order`.`payment_lastname`,
                              `order`.`payment_company`,
                              `order`.`payment_address_1`,
                              `order`.`payment_address_2`,
                              `order`.`payment_city`,
                              `order`.`payment_postcode`,
                              `order`.`payment_country`,
                              `order`.`payment_country_id`,
                              `order`.`payment_zone`,
                              `order`.`payment_zone_id`,
                              `order`.`payment_address_format`,
                              `order`.`payment_custom_field`,
                              `order`.`payment_method`,
                              `order`.`payment_code`,
                              `order`.`shipping_firstname`,
                              `order`.`shipping_lastname`,
                              `order`.`shipping_company`,
                              `order`.`shipping_address_1`,
                              `order`.`shipping_address_2`,
                              `order`.`shipping_city`,
                              `order`.`shipping_postcode`,
                              `order`.`shipping_country`,
                              `order`.`shipping_country_id`,
                              `order`.`shipping_zone`,
                              `order`.`shipping_zone_id`,
                              `order`.`shipping_address_format`,
                              `order`.`shipping_custom_field`,
                              `order`.`shipping_method`,
                              `order`.`shipping_code`,
                              `order`.`comment`,
                              `order`.`total`,
                              2 as order_status_id,
                              `order`.`affiliate_id`,
                              `order`.`commission`,
                              `order`.`marketing_id`,
                              `order`.`tracking`,
                              `order`.`language_id`,
                              `order`.`currency_id`,
                              `order`.`currency_code`,
                              `order`.`currency_value`,
                              `order`.`ip`,
                              `order`.`forwarded_ip`,
                              `order`.`user_agent`,
                              `order`.`accept_language`,
                              `order`.`date_added`,
                              `order`.`date_modified`,
                              `order`.`news_week_id`,
                              `order`.`purchase_type`,
                              `order`.`repproved_comment`,
                              `order`.`is_save_cart`,
                              `order`.`datetime_invoice`
                          FROM `webstore`.`order`
                          where order_id = '".$pedido."' and news_week_id = 163;";

                  DB::insert($sql);

                  $pedido_2 = DB::getPdo()->lastInsertId();

                  if((int)$pedido_2<1)
                  {
                      $this->line('ERRO AO CRIAR O PEDIDO 1');

                      return false;
                  }

                  $this->line('Pedido['.$pedido_2.'] gerado com sucesso!');

                  $update2 = "UPDATE webstore.order_product op
                              join product p on p.product_id = op.product_id
                              SET op.order_id = '".$pedido_2."'
                              WHERE op.order_id = '".$pedido."'
                              AND p.model IN (
                                '13-147-NCL-00827-GD-GN',
                                '13-414-ERR-00376-GD-GN-CR',
                                '13-414-ERR-00376-OR-GN-CR',
                                '13-149-ERR-03187-GD-MU',
                                '13-149-ERR-03187-OR-MU',
                                '13-149-ERR-03187-BK-MU',
                                '13-149-ERR-03153-GD-MU',
                                '13-149-ERR-03153-OR-MU',
                                '13-414-BRC-00445-GD-MU',
                                '13-415-ERR-00534-GD',
                                '13-415-ERR-00534-OR',
                                '13-418-ERR-01228-GD',
                                '13-418-ERR-01228-OR',
                                '13-224-ERR-00016-GD',
                                '13-224-ERR-00016-OR',
                                '13-153-ERR-00098-GD',
                                '13-153-ERR-00098-OR',
                                '13-501-NCL-00173-GD-CR',
                                '13-501-NCL-00173-OR-CR',
                                '13-501-NCL-00224-GD-CR',
                                '13-501-NCL-00224-OR-CR',
                                '13-020-NCL-00967-GD-CR',
                                '13-414-ERR-00380-GD-CR',
                                '13-414-ERR-00380-OR-CR',
                                '13-501-NCL-00227-GD-CR',
                                '13-501-NCL-00227-OR-CR',
                                '13-149-ERR-03015-GD-CR',
                                '13-149-ERR-03015-OR-CR',
                                '13-149-ERR-03015-BK-CR',
                                '13-415-NCL-00573-GD-PL',
                                '13-415-NCL-00573-OR-PL',
                                '13-057-ERR-00036-GD-PL',
                                '13-057-ERR-00036-OR-PL',
                                '13-415-BRC-00574-GD-PL',
                                '13-415-BRC-00574-OR-PL',
                                '13-623-BRC-00316-GD',
                                '13-411-NCL-01412-GD-CR',
                                '13-411-NCL-01412-OR-CR',
                                '13-411-ERR-01413-GD-CR',
                                '13-411-ERR-01413-OR-CR',
                                '13-149-NCL-03168-GD-BD',
                                '13-149-NCL-03168-BK-BD',
                                '13-195-ERR-00434-GD-CR',
                                '13-195-ERR-00434-OR-CR',
                                '13-195-ERR-00434-BK-CR',
                                '13-149-ERR-03166-GD-BD',
                                '13-149-ERR-03166-BK-BD',
                                '13-149-NCL-03150-GD-CR',
                                '13-149-NCL-03150-OR-CR',
                                '13-149-ERR-03149-GD-CR',
                                '13-149-ERR-03149-OR-CR',
                                '13-501-NCL-00225-GD-CR',
                                '13-501-NCL-00225-OR-CR',
                                '13-501-NCL-00077-GD-CR',
                                '13-501-NCL-00077-OR-CR',
                                '13-501-BRC-00140-GD-CR',
                                '13-501-BRC-00140-OR-CR',
                                '13-501-NCL-00267-GD-CR',
                                '13-501-NCL-00267-OR-CR',
                                '13-004-NCL-00352-GD-CR',
                                '13-004-NCL-00352-OR-CR');";

                              DB::insert($update2);

                            $this->line('Atualizado o  Pedido['.$pedido_2.']');

                            $update_principal = "UPDATE webstore.order
                                                SET invoice_no = 3,
                                                order_status_id = 2
                                                where news_week_id = 163 AND order_id = '".$pedido."';";

                            DB::insert($update_principal);

                            $this->line('Fim da loja '.$order->firstname);
      }




    }
}
