<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class morana_csm_email_historico_semana extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-csm-email-historico-semana';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Envia email do relatório de itens na caixa com as ultimos pedidos de CSM';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    ini_set("memory_limit","2048M");

    //  $file = env('PATH_REPORT_FILE')."/morana_csm_caixa_historico_semana.csv";
    //  $myfile = fopen($file, "w") or die("Unable to open file!");

    $sql = "  SELECT
    u.firstname as supervisor,
    u.email,
    u.user_id
    FROM customer as c
    LEFT JOIN user as u ON u.user_id = c.supervisor_id
    WHERE c.ppe_access = 1
    AND u.user_id > 0
    GROUP BY 1,2,3";

    $consultores =  DB::select($sql);

    $semanas = $this->getUltimas4SemanasCsm();

    sort($semanas);

    foreach($consultores as $consultor)
    {

      $this->line('Coletando Lojas do consultor - '.$consultor->email);

      $sql = "  SELECT
      c.customer_id,
      c.brazil_store_id,
      c.brazil_store_name,
      u.firstname as supervisor,
      u.email,
      u.user_id
      FROM customer as c
      LEFT JOIN user as u ON u.user_id = c.supervisor_id
      WHERE c.ppe_access = 1
      AND u.user_id = '".$consultor->user_id."'
      ORDER BY u.user_id ASC LIMIT 10";

      $customers =  DB::select($sql);

      $data['caixa'] = array();

      $data['consultor'] = $consultor->supervisor;
      $data['email'] =  $consultor->email;

      foreach($customers as $customer)
      {
        //      $this->line('Itens na caixa de  - '.$customer->brazil_store_name.' '.date('d-m-Y H:i:s'));

        $data['caixa'][$customer->customer_id]['area']=$customer->brazil_store_id;
        $data['caixa'][$customer->customer_id]['loja']=$customer->brazil_store_name;
        $data['caixa'][$customer->customer_id]['supervisor']=$customer->supervisor;
        $data['caixa'][$customer->customer_id]['email']=$customer->email;
        $data['caixa'][$customer->customer_id]['user_id']=$customer->user_id;

        $itens_caixa = $this->getItensCaixaCustomer($customer->customer_id);

        if(count( $itens_caixa ) > 0)
        {
          $data['caixa'][$customer->customer_id]['caixa']=$itens_caixa[0]->qtd;
        }
        else
        {
          $data['caixa'][$customer->customer_id]['caixa']=0;
        }

        foreach($semanas as $semana)
        {
          $data['caixa'][$customer->customer_id][$semana->news_week_id] = $this->getQuantidadeSemana($customer->customer_id, $semana->news_week_id);
        }

      }

      $data['semanas'] = $semanas;

      $this->mailConsultor($data);

    }
  }

  public function mailConsultor($data)
  {

    $view = 'mail/email_historico_consultor';

    //  $data['emails'][] = 'fabio.liossi@gmail.com';

    if(count($data['email']) > 0){
      Mail::send($view, $data, function($message) use ($data) {
        $message->to($data['email'],'')->subject( '[MORANA] Produtos na Caixa ['.date('d-m-Y').']'.$data['consultor'] );
        $message->from('dev@grupoornatus.com','[MORANA]');
      });
    }else{
      return true;
    }
  }

  public function getItensCaixaCustomer($customer_id)
  {
    $sql = "SELECT customer_id, SUM(quantidade) AS qtd
    FROM (
      SELECT
      'CSM' AS plataforma,
      o.customer_id,
      date(o.date_added) as date_added,
      o.order_id AS pedido,
      p.product_id,
      p.model,
      p.brazil_code, SUBSTRING(p.model,8,3) AS categoria,
      fp.suggested_price,
      pd.brazil_description AS descricao,
      ps.name as product_name,
      s1.subcategory_name AS sub1_name,
      s1.subcategory_id,
      s2.subcategory_name AS sub2_name,
      s2.subcategory_id subcategory2_id,
      se.segment_name AS segmento,
      se.segment_id,
      (COALESCE(op.quantity,0) + COALESCE(op.extra_quantity,0)) AS quantidade
      FROM webstore.order_product op
      JOIN webstore.order o ON o.order_id = op.order_id
      JOIN product p ON p.product_id = op.product_id
      JOIN product_fixed_price_brazil fp ON fp.product_id = p.product_id
      JOIN customer c ON c.customer_id = o.customer_id
      JOIN product_description pd ON pd.product_id = p.product_id AND pd.language_id = 3
      LEFT JOIN product_sub_category ps ON ps.product_id = p.product_id
      LEFT JOIN subcategory s1 ON s1.subcategory_id = ps.subcategory_id
      LEFT JOIN subcategory s2 ON s2.subcategory_id = ps.sub_category2_id
      LEFT JOIN segment se ON se.segment_id = ps.segment_id
      WHERE o.customer_id = '".$customer_id."' AND  o.purchase_type IN ('A','N') AND o.order_status_id = 3 AND (op.quantity>0 OR op.extra_quantity>0) UNION
      SELECT
      'CSM' AS plataforma,
      o.customer_id,
      date(o.date_added) as date_added,
      o.order_id AS pedido,
      p.product_id,
      p.model,
      p.brazil_code, SUBSTRING(p.model,8,3) AS categoria,
      fp.suggested_price,
      pd.brazil_description AS descricao,
      ps.name as product_name,
      s1.subcategory_name AS sub1_name,
      s1.subcategory_id,
      s2.subcategory_name AS sub2_name,
      s2.subcategory_id subcategory2_id,
      se.segment_name AS segmento,
      se.segment_id,
      COALESCE(op.quantity_partial,0) AS quantidade
      FROM webstore.order_product op
      JOIN webstore.order o ON o.order_id = op.order_id
      JOIN product p ON p.product_id = op.product_id
      JOIN product_fixed_price_brazil fp ON fp.product_id = p.product_id
      JOIN customer c ON c.customer_id = o.customer_id
      JOIN product_description pd ON pd.product_id = p.product_id AND pd.language_id = 3
      LEFT JOIN product_sub_category ps ON ps.product_id = p.product_id
      LEFT JOIN subcategory s1 ON s1.subcategory_id = ps.subcategory_id
      LEFT JOIN subcategory s2 ON s2.subcategory_id = ps.sub_category2_id
      LEFT JOIN segment se ON se.segment_id = ps.segment_id
      WHERE  o.customer_id = '".$customer_id."' AND o.purchase_type IN ('R') AND o.order_status_id = 3 AND (op.quantity_partial>0) UNION
      SELECT
      'LV' AS plataforma,
      o.customer_id,
      date(o.date_added) as date_added,
      o.lv_order_id AS pedido,
      p.product_id,
      p.model,
      p.brazil_code, SUBSTRING(p.model,8,3) AS categoria,
      fp.suggested_price,
      pd.brazil_description AS descricao,
      ps.name as product_name,
      s1.subcategory_name AS sub1_name,
      s1.subcategory_id,
      s2.subcategory_name AS sub2_name,
      s2.subcategory_id subcategory2_id,
      se.segment_name AS segmento,
      se.segment_id,
      COALESCE(op.quantity_partial,0) AS quantidade
      FROM lv_order_product op
      JOIN lv_order o ON o.lv_order_id = op.lv_order_id
      JOIN product p ON p.product_id = op.product_id
      JOIN product_fixed_price_brazil fp ON fp.product_id = p.product_id
      JOIN customer c ON c.customer_id = o.customer_id
      JOIN product_description pd ON pd.product_id = p.product_id AND pd.language_id = 3
      LEFT JOIN product_sub_category ps ON ps.product_id = p.product_id
      LEFT JOIN subcategory s1 ON s1.subcategory_id = ps.subcategory_id
      LEFT JOIN subcategory s2 ON s2.subcategory_id = ps.sub_category2_id
      LEFT JOIN segment se ON se.segment_id = ps.segment_id
      WHERE  o.customer_id = '".$customer_id."' AND o.status_id = 3 AND (op.quantity_partial>0) UNION
      SELECT
      'PPM' AS plataforma,
      o.customer_id,
      date(o.date_added) as date_added,
      op.week_number AS pedido,
      p.product_id,
      p.model,
      p.brazil_code, SUBSTRING(p.model,8,3) AS categoria,
      fp.suggested_price,
      pd.brazil_description AS descricao,
      ps.name as product_name,
      s1.subcategory_name AS sub1_name,
      s1.subcategory_id,
      s2.subcategory_name AS sub2_name,
      s2.subcategory_id subcategory2_id,
      se.segment_name AS segmento,
      se.segment_id,
      COALESCE(op.quantity,0) AS quantidade
      FROM order_product op
      JOIN `order` o ON o.order_id = op.order_id
      JOIN product p ON p.product_id = op.product_id
      JOIN product_fixed_price_brazil fp ON fp.product_id = p.product_id
      JOIN customer c ON c.customer_id = o.customer_id
      JOIN product_description pd ON pd.product_id = p.product_id AND pd.language_id = 3
      LEFT JOIN product_sub_category ps ON ps.product_id = p.product_id
      LEFT JOIN subcategory s1 ON s1.subcategory_id = ps.subcategory_id
      LEFT JOIN subcategory s2 ON s2.subcategory_id = ps.sub_category2_id
      LEFT JOIN segment se ON se.segment_id = ps.segment_id
      WHERE o.customer_id = '".$customer_id."' AND op.order_product_status_id = 8 AND (op.quantity>0)
    ) AS ped GROUP BY 1";

    $results =  DB::select($sql);

    return $results;
  }

  public function getUltimas4SemanasCsm()
  {
    $sql = "SELECT news_week_id FROM news_week
    WHERE confirmed = 'S'
    AND date_end < current_date
    ORDER BY 1 DESC LIMIT 4;";
    $results =  DB::select($sql);

    return $results;
  }

  public function getQuantidadeSemana($customer_id, $news_week_id)
  {
    $sql = "SELECT news_week_id, SUM(qtd) as qtd
    FROM
    (
      SELECT o.news_week_id,
      sum(COALESCE(quantity,0) + COALESCE(extra_quantity,0)) AS qtd
      FROM webstore.order_product op
      JOIN webstore.order o ON o.order_id = op.order_id
      WHERE o.purchase_type IN ('A','N')
      AND o.news_week_id = '".$news_week_id."'
      AND o.customer_id = '".$customer_id."'
      GROUP BY 1
      UNION
      SELECT o.news_week_id,
      sum(COALESCE(quantity_partial,0)) AS qtd
      FROM webstore.order_product op
      JOIN webstore.order o ON o.order_id = op.order_id
      WHERE o.purchase_type = 'R'
      AND o.news_week_id = '".$news_week_id."'
      AND o.customer_id = '".$customer_id."'
      GROUP BY 1 ) AS t
      GROUP BY 1";

      $results =  DB::select($sql);

      $qtd = 0;

      if(count($results) > 0)
      {
        $qtd = $results[0]->qtd;
      }

      return $qtd;

    }

  }
