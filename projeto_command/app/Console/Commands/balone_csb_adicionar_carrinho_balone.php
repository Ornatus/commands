<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class balone_csb_adicionar_carrinho_balone extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:balone-csb-adicionar-carrinho-balone';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Adiciona carrinho balone';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    $sql = "delete from balone.cart;";

    DB::select($sql);

    $sql = "SELECT * FROM balone_customer c
    JOIN balone_customer_profile cp
    ON cp.balone_customer_profile_id = c.customer_profile_id
    WHERE c.ppe_access = '1' AND rede = '1' ";

    $customers = DB::select($sql);

    foreach ($customers as $customer)
    {

      //Pega os produtos da semana atual*************************
      $sql = "SELECT
      nwp.*,
      (SELECT quantity FROM balone_news_week_product_profile
        WHERE active = 1
        AND news_week_product_id = nwp.news_week_product_id
        AND profile_slug='".$customer->slug."'
        LIMIT 1) as profile_quantity,
        (SELECT
          COALESCE(SUM(quantity),0) + COALESCE(SUM(extra_quantity),0) AS quantity
          FROM balone.order_product op
          JOIN balone.order o ON o.order_id = op.order_id
          WHERE o.news_week_id = nwp.news_week_id
          AND op.product_id = nwp.product_id
          AND o.purchase_type IN('N','A') LIMIT 1) AS orders
        FROM balone_news_week_product nwp
        JOIN balone_news_week nw ON nw.news_week_id = nwp.news_week_id
        JOIN balone_news_week_family nwf ON nwf.news_week_id = nw.news_week_id AND nwp.product_family_id = nwf.product_family_id AND nwf.active = 1
        JOIN product p ON nwp.product_id = p.product_id
        WHERE
        nwp.active = 1
        AND
        current_date >= nw.date_start
        AND
        current_date <= nw.date_end
        AND nw.confirmed = 'S'
        AND nw.active = 1
        ORDER BY nwp.address_index;";

        $products = DB::select($sql);

        if(count($products) == 0)
        {
          return false;
        }
        //***********************************************************

        $cart = array();


        foreach($products as $product)
        {
          $profile_config = array('++' => 3,'+' => 2,'+-' => 1,'-' => 1,'--' => 1 );
          $profile_quantity = 0;

          $new_quantity = (int)$product->quantity - (int)$product->orders;

          if($new_quantity >= 20){
            $profile_quantity = $profile_config[$customer->slug];
          }

          $cart['customer_id'] = $customer->customer_id;
          $cart['product_id']  = $product->product_id;
          $cart['quantity']    = $new_quantity;
          //$cart['quantity']    = $product->profile_quantity;
          $cart['profile_quantity']    = $profile_quantity;
          $cart['news_week_id']    = $product->news_week_id;
          $cart['purchase_type']    = 'N';
          $cart['type'] = $product->type;

          $sql = "INSERT INTO balone.cart
          SET
          customer_id = '".$cart['customer_id']."',
          product_id = '".$cart['product_id']."',
          quantity = '".$cart['profile_quantity']."',
          profile_quantity = '".$cart['profile_quantity']."',
          purchase_type = '".$cart['purchase_type']."',
          news_week_id = '".$cart['news_week_id']."',
          type = '".$cart['type']."',
          date_added = current_timestamp,
          `option` = '[]'; ";

          DB::insert($sql);
        }

      }

      $this->line('Fim');


    }
  }
