<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class morana_csm_importa_produtos_semana extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:morana-csm-importa-produtos-semana';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa os dados dos produtos da semana confirmada no CSM para o banco webstore';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->line('Truncando a tabela webstore.produtos_semana.  '.date('d-m-Y H:i:s'));

      $sql = "truncate table webstore.produtos_semana;";

      DB::select($sql);


      $this->line('Inserindo em webstore.produtos_semana.  '.date('d-m-Y H:i:s'));

      $sql = "INSERT INTO webstore.produtos_semana
          SELECT
              nwp.product_id,
              nwp.quantity AS quantity_confirmed,
              nwp.quantity_over,
              (SELECT name FROM morana2.product_family WHERE product_family_id = nwp.product_family_id LIMIT 1) as family_name,
              (0) AS pedido_ppm,
              (SELECT
                      fixed_price + ((suggested_price * 5) / 100)
                  FROM
                      morana2.product_fixed_price_brazil
                  WHERE
                      product_id = p.product_id) AS franchise_price,
                      (SELECT
                      suggested_price
                  FROM
                      morana2.product_fixed_price_brazil
                  WHERE
                      product_id = p.product_id) AS retail_price,

              (SELECT
                      fixed_price
                  FROM
                      morana2.product_fixed_price_brazil
                  WHERE
                      product_id = p.product_id) AS employe_price,
              (SELECT
                      is_publicity
                  FROM
                      morana2.product_family
                  WHERE
                      product_family_id = nwp.product_family_id
                  LIMIT 1) AS is_publicity,
              (SELECT
                      description
                  FROM
                      morana2.product_description pd
                  WHERE
                      language_id = 3
                          AND pd.product_id = p.product_id
                  LIMIT 1) AS product_description,
              (SELECT
                      cd.name
                  FROM
                      morana2.product_to_category ptc
                          JOIN
                      morana2.category_description cd ON cd.category_id = ptc.category_id
                  WHERE
                      ptc.product_id = p.product_id
                          AND cd.language_id = 3
                  ORDER BY ptc.category_id
                  LIMIT 1) AS category_description,
              p.image,
              p.brazil_code,
              nwf.priority,
              p.model,
              p.shipping,
              p.minimum,
              p.subtract,
              p.points,
              p.tax_class_id,
              p.weight,
              p.weight_class_id,
              p.length,
              p.width,
              p.height,
              p.length_class_id

          FROM
              product p
                  JOIN
              morana2.news_week_product nwp ON nwp.product_id = p.product_id
                  JOIN
              news_week nw ON nw.news_week_id = nwp.news_week_id
                  JOIN
              morana2.news_week_family AS nwf ON nwf.product_family_id = nwp.product_family_id
                  AND nwf.news_week_id = nwp.news_week_id
                  AND nwf.active = 1
          WHERE
              nwp.active = 1
          AND nw.date_start <= CURRENT_DATE
          AND nw.date_end >= CURRENT_DATE
          AND nw.confirmed = 'S';";

          $products = DB::insert($sql);

    }
}
