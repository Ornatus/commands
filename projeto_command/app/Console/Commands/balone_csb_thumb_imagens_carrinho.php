<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class balone_csb_thumb_imagens_carrinho extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:balone-csb-thumb-imagens-carrinho';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria thumbnails do carrinho de produtos CSB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->line('Pegando Produtos');

      $sql = "SELECT
      p.product_id,
      p.model,
      p.image from balone.cart c
      join morana2.product p on p.product_id = c.product_id
      group by 1,2,3;";

      $products = DB::select($sql);

      $this->line('Coletando imagem por product_id');

      //imagens principais
      foreach($products as $product)
      {
        $url = sprintf(env('HTTP_CREATE_IMAGE'),500,500,$product->image);

        $name = env('PATH_THUMB_BALONE').'CSB/200/'.$product->product_id.'.jpg';

        echo $name."\r\n";
        echo $url."\r\n";

        $conteudo = @file_get_contents($url);

        if(strlen($conteudo) < 100){
          continue;
        }

        if(!file_exists($name))
        {
          file_put_contents($name, $conteudo);
        }

      }

      $this->line('Coletando imagens extras por product_id');
      //imagens extras
      foreach($products as $product)
      {

        $sql = "SELECT * FROM morana2.product_image
    		WHERE  product_id = '".$product->product_id."'
    		ORDER BY sort_order";
        $images = DB::select($sql);

        $this->line("Product ID: ".$product->product_id);

        foreach ($images as $image) {

          $image_edited = preg_replace('@data/\d+/@is','',$image->image);

          $url = sprintf(env('HTTP_CREATE_IMAGE'),500,500,$image->image);

          $name = env('PATH_THUMB_BALONE').'CSB/200/'.$image_edited;

          echo $name."\r\n";
          echo $url."\r\n";

          $conteudo = @file_get_contents($url);

          if(strlen($conteudo) < 100){
            continue;
          }

          if(!file_exists($name))
          {
            file_put_contents($name, $conteudo);
          }

        }



      }
    }
}
