<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class morana_lv_desabilita_produtos_campanha extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-lv-desabilita-produtos-campanha';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Inativa Produtos de Campanha Loja Virtual';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    $this->line('Iniciando inativacao - '.date('d-m-Y H:i:s'));

    $sql = "UPDATE lv_product
    set
    is_categoria = 0,
    is_campanha = 0
    WHERE is_categoria = 1
    AND is_campanha = 1;  ";

    $return = DB::update($sql);

    if($return == 0)
    {
      $this->line('Falha ao inativar ptodutos - '.date('d-m-Y H:i:s'));
    }else
    {
      $this->line('Produtos Inativados com sucesso - '.date('d-m-Y H:i:s'));
    }

  }
}
