<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class morana_sga_envia_email_alteracao_preco_produto extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-sga-envia-email-alteracao-preco-produto';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Envia emails com as alterações dos preços dos produtos';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    $this->line('Coletando logs novos: '.date('d-m-Y H:i:s'));

    $sql = "SELECT
    l.*,
    p.model,
    u.firstname,
    u.lastname
    FROM log_product_price as l
    JOIN product as p ON p.product_id = l.product_id
    JOIN user as u ON u.user_id = l.user_id
    WHERE l.email = 0";

    $products =  DB::select($sql);

    if(count($products) < 1){
      $this->line('Nenhum produto alterado : '.date('d-m-Y H:i:s'));
      return false;
    }

    $this->line('Montando HTML : '.date('d-m-Y H:i:s'));

    $view = 'mail/products_alter_price_list';

    $html = "";

    $html .='<table border=1 class="table table-bordred table-striped">
    <tr>
    <th>Modelo</th>
    <th>Old Fixed Price</th>
    <th>New Fixed Price</th>
    <th>Old Suggested Price</th>
    <th>New Suggested Price</th>
    <th>Usuário</th>
    <th>Origem</th>
    <th>Data</th>
    </tr>';

    foreach ($products as $product) {
      $html .="<tr>";
      $html .="<td>".$product->model."</td>";
      $html .="<td>".$product->old_fixed_price."</td>";
      $html .="<td>".$product->new_fixed_price."</td>";
      $html .="<td>".$product->old_suggested_price."</td>";
      $html .="<td>".$product->new_suggested_price."</td>";
      $html .="<td>".$product->firstname.' '.$product->lastname."</td>";

      if($product->type == 'P'){
        $html .="Tela de Preço</td>";
      }else{
        $html .="Formulário</td>";
      }

      $html .="<td>".date("d/m/Y",strtotime($product->date_added))."</td>";
      $html .="</tr>";

      $sql = "UPDATE log_product_price
      SET
      email = 1
      WHERE product_id = '".$product->product_id."' ";

      DB::update($sql);

    }

    $html .="</table>";

    $data['html'] = $html;

    $emails = env('EMAILS_LOG_PRODUCT_HISTORY');
    $data['emails'] = explode(",",$emails);

    $this->line('Enviando emails: '.date('d-m-Y H:i:s'));

    Mail::send($view, $data, function($message) use ($data) {
      $message->to($data['emails'],'')->subject( '[SGA] Alteração no preço de produtos' );
      $message->from('dev@grupoornatus.com','[SGA]');
    });



  }
}
