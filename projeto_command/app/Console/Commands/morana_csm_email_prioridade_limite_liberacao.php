<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class morana_csm_email_prioridade_limite_liberacao extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-csm-email-prioridade-limite-liberacao';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Envia email para lojas com pedidos perto da data limite de midia ';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    $days = 5;

    $this->line('Coletando pedidos com '.$days.' dias do prazo limite '.date('d-m-Y H:i:s'));

    $orders = $this->getOrders($days);

    $this->line('Iniciando envio de emails Dias '.date('d-m-Y H:i:s'));

    foreach ($orders as $order)
    {
      $this->line('Pedido:'.$order->order_id.' Semana:'.$order->news_week_id.' '.date('d-m-Y H:i:s'));

      $data = array();
      $data['emails'] = array();
      $data['info'] = $order;

      $view = 'mail/email_prioridade_limit';

      
      if(isset($order->email_sup) && strlen($order->email_sup) > 3)
      {
        $data['emails'][] = $order->email_sup;
      }

      if(isset($order->email) && strlen($order->email) > 3)
      {
        $data['emails'][] = $order->email;
      }


      $sql = "SELECT email1,email2,email3 FROM customer_email WHERE customer_id = '".$order->customer_id."' ";
      $customer_emails =  DB::select($sql);

      if(count($customer_emails) > 0)
      {
        if(isset($customer_emails[0]->email1) && strlen($customer_emails[0]->email1) > 3)
        {
          $data['emails'][] = $customer_emails[0]->email1;
        }

        if(isset($customer_emails[0]->email2) && strlen($customer_emails[0]->email2) > 3)
        {
          $data['emails'][] = $customer_emails[0]->email2;
        }

        if(isset($customer_emails[0]->email3) && strlen($customer_emails[0]->email3) > 3)
        {
          $data['emails'][] = $customer_emails[0]->email3;
        }
      }

      $data['emails'][] = 'devmorana@gmail.com';

      if(count($data['emails']) > 0)
      {
        Mail::send($view, $data, function($message) use ($data) {
          $subject ='MORANA - LIBERAÇÃO PRIORIDADE '.$data['info']->priority.' - CSM '.$data['info']->news_week_id.'(Período entre '. date("d/m/Y", strtotime($data['info']->data_inicio_csm)).' à '.date("d/m/Y", strtotime($data['info']->data_fim_csm)).') - Mídia '.date("d/m/Y", strtotime($data['info']->data_inicio_midia)).'';
          $message->to($data['emails'],'')->subject( (string)$subject );
          $message->from('dev@grupoornatus.com','[MORANA]');
        });
      }


    }


    //  Inicia com outro quantidade de dias
    $days = 0;

    $this->line('Coletando pedidos com '.$days.' dias do prazo limite '.date('d-m-Y H:i:s'));

    $orders = $this->getOrders($days);

    $this->line('Iniciando envio de emails Dias '.date('d-m-Y H:i:s'));

    foreach ($orders as $order)
    {
      $this->line('Pedido:'.$order->order_id.' Semana:'.$order->news_week_id.' '.date('d-m-Y H:i:s'));

      $data = array();
      $data['emails'] = array();
      $data['info'] = $order;

      $view = 'mail/email_prioridade_limit_2';


      if(isset($order->email_sup) && strlen($order->email_sup) > 3)
      {
        $data['emails'][] = $order->email_sup;
      }

      if(isset($order->email) && strlen($order->email) > 3)
      {
        $data['emails'][] = $order->email;
      }

      $sql = "SELECT email1,email2,email3 FROM customer_email WHERE customer_id = '".$order->customer_id."' ";
      $customer_emails =  DB::select($sql);

      if(count($customer_emails) > 0)
      {
        if(isset($customer_emails[0]->email1) && strlen($customer_emails[0]->email1) > 3)
        {
          $data['emails'][] = $customer_emails[0]->email1;
        }

        if(isset($customer_emails[0]->email2) && strlen($customer_emails[0]->email2) > 3)
        {
          $data['emails'][] = $customer_emails[0]->email2;
        }

        if(isset($customer_emails[0]->email3) && strlen($customer_emails[0]->email3) > 3)
        {
          $data['emails'][] = $customer_emails[0]->email3;
        }
      }

      $data['emails'][] = 'devmorana@gmail.com';

      if(count($data['emails']) > 0)
      {
        Mail::send($view, $data, function($message) use ($data) {
          $subject ='MORANA - LIMITE DE DATA DE LIBERAÇÃO  '.$data['info']->priority.' - CSM '.$data['info']->news_week_id.'(Período entre '. date("d/m/Y", strtotime($data['info']->data_inicio_csm)).' à '.date("d/m/Y", strtotime($data['info']->data_fim_csm)).') - Mídia '.date("d/m/Y", strtotime($data['info']->data_inicio_midia)).'';
          $message->to($data['emails'],'')->subject( (string)$subject );
          $message->from('dev@grupoornatus.com','[MORANA]');
        });
      }


    }

  }


  public function getOrders($days)
  {
    $sql = "SELECT
    nwp.news_week_id,
    o.order_id,
    n.date_start as data_inicio_csm,
    n.date_end as data_fim_csm,
    nwp.date_start as data_inicio_midia,
    nwp.date_end as data_fim_midia,
    r.dias_frete,
    DATE_SUB(nwp.date_start, INTERVAL r.dias_frete DAY)  as data_limit,
    datediff(DATE_SUB(nwp.date_start, INTERVAL r.dias_frete DAY),current_date()) as dias_prazo,
    nwp.priority,
    c.customer_id,
    c.email,
    c.brazil_store_id,
    c.brazil_store_name,
    r.descricao,
    u.email as email_sup,
    u.username,
    (
      SELECT oo.lv_confirm_picking_id
      FROM lv_confirm_picking_orders oo
      JOIN lv_confirm_picking lv ON lv.lv_confirm_picking_id = oo.lv_confirm_picking_id
      WHERE oo.type_system='csm'
      AND oo.order_id=o.order_id
      AND oo.active = 1
      AND lv.active = 1
      LIMIT 1
    ) AS liberacao
    FROM news_week_priority as nwp
    JOIN news_week as n ON n.news_week_id = nwp.news_week_id
    JOIN webstore.order as o ON o.news_week_id = nwp.news_week_id AND o.invoice_no = nwp.priority AND o.purchase_type IN ('N','A')
    JOIN customer as c ON c.customer_id = o.customer_id
    JOIN regional as r ON r.regional_id = c.regional_id
    JOIN user as u ON u.user_id = c.supervisor_id
    where nwp.date_start >= current_date()
    AND o.order_status_id = 3
    HAVING dias_prazo = '".$days."' AND liberacao is null LIMIT 1;";

    return DB::select($sql);
  }

}
