<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class morana_ppm_atualiza_pedidos_faturados extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-ppm-atualiza-pedidos-faturados';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Command description';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {

    $this->line('Iniciando atualizacao de pedidos ppm '.date('d-m-Y H:i:s'));

    $sql = "SELECT
    pedidos.order_product_id,
    r.file_date,
    r.romaneio_ppm_id,
    r.name
    from romaneio_ppm as r
    JOIN (SELECT
      op.order_product_id,
      c.brazil_store_id,
      p.product_id,
      p.brazil_code,
      op.week_number
      FROM order_product as op
      JOIN `order` as o ON o.order_id = op.order_id
      JOIN customer as c ON c.customer_id = o.customer_id
      JOIN product as p ON p.product_id = op.product_id
      WHERE op.order_product_status_id = 8) as pedidos ON pedidos.brazil_store_id = r.file_store  AND pedidos.brazil_code = r.file_product_id AND pedidos.week_number = r.week_formated
      where r.processado = 1";

      $orders_products =  DB::select($sql);

      $this->line('Iniciando insert order_product_invoice '.date('d-m-Y H:i:s'));

      foreach ($orders_products as $orders_product) {

        $sql = "INSERT INTO order_product_invoice
        SET
        order_product_id = '" . $orders_product->order_product_id . "',
        name_file = '" . $orders_product->name . "',
        date_file = '" . $orders_product->file_date . "' ";

        DB::insert($sql);

        $sql = "UPDATE order_product
        SET order_product_status_id = 13
        WHERE order_product_id =  '" . $orders_product->order_product_id . "' ";

        DB::update($sql);

        $sql = "UPDATE romaneio_ppm
        SET processado = 2,
        data_processado = current_timestamp,
        STATUS = 'C'
        WHERE romaneio_ppm_id = '" . $orders_product->romaneio_ppm_id . "' ";

        DB::update($sql);

      }

      $sql = "UPDATE romaneio_ppm
      SET processado = 2,
      STATUS = 'D'
      WHERE processado = 1";

      DB::update($sql);


      $this->line('Command finalizado '.date('d-m-Y H:i:s'));

    }


  }
