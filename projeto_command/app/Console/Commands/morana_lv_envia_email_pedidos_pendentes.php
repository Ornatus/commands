<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class morana_lv_envia_email_pedidos_pendentes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:morana-lv-envia-email-pedidos-pendentes {daysFrom} {daysTo} {dateAdded}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $daysFrom = $this->argument('daysFrom');
        $daysTo = $this->argument('daysTo');
        $dateAdded = $this->argument('dateAdded');
        
        $this->line(date('d-m-Y H:i:s') . ' - Selecionando pedidos pendentes entre ' . $daysFrom . ' e ' . $daysTo . ' dias a partir de ' . $dateAdded);

        $sql = "SELECT 
            o.lv_order_id, 
            l.lv_confirm_picking_id, 
            datediff(NOW(), o.date_added) AS dias, 
            c.customer_id,
            c.email, 
            c.brazil_store_name,
            u.email AS supervisor_email,
            SUM(op.quantity) AS pecas, 
            (SUM(op.price) * op.quantity) AS valor_pedido
        FROM morana2.lv_order o
        JOIN morana2.lv_order_product op ON op.lv_order_id = o.lv_order_id
        JOIN morana2.customer c ON c.customer_id = o.customer_id AND o.type_user_id = 1
        LEFT JOIN morana2.user u on u.user_id = c.supervisor_id
        LEFT JOIN (
            SELECT cpo.order_id, cpo.lv_confirm_picking_id, cpo.type_system
            FROM lv_confirm_picking_orders cpo 
            JOIN lv_confirm_picking cp ON cp.lv_confirm_picking_id = cpo.lv_confirm_picking_id
            WHERE cp.active = 1
            AND cpo.active = 1
            AND cpo.type_system = 'lv'
        ) AS l ON l.order_id = o.lv_order_id
        WHERE l.lv_confirm_picking_id IS NULL
        AND DATE(o.date_added) >= '" . $dateAdded . "'
        AND datediff(NOW(), o.date_added) BETWEEN " . $daysFrom . " AND " . $daysTo . "
        GROUP BY 1, 2, 3, 4, 5, 6, 7";

        $orders = DB::select($sql);

        if (count($orders) < 1) {
            $this->line(date('d-m-Y H:i:s') . ' - Nenhum pedido pendente entre 5 e 10 dias');
            return false;
        }

        $ordersByStore = array();
        foreach ($orders as $order) {
            $ordersByStore[$order->customer_id][] = $order;
        }

        foreach ($ordersByStore as $orders) {
            $data = array();
            $data['emails'] = array();
            $data['orders'] = $orders;

            $view = 'mail/email_pedidos_pendentes';

            $data['emails'][] = $orders[0]->email;
            $data['emails'][] = 'devmorana@gmail.com';

            if (isset($orders->supervisor_email) && strlen($orders->supervisor_email) > 3 ){
                $data['emails'][] = $orders[0]->supervisor_email;
            }

            if(count($data['emails']) > 0) {
                Mail::send($view, $data, function($message) use ($data) {
                    $subject = 'Pedidos Pendentes';
                    $message->to($data['emails'], '')->subject( (string)$subject );
                    $message->from('dev@grupoornatus.com','[MORANA]');
                });

                $this->line(date('d-m-Y H:i:s') . " - Emails enviado para \n" . implode("\n", $data['emails']));
            }
            
        }
    }
}
