<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class morana_itens_caixa extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-itens-caixa';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Command description';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {

    ini_set("memory_limit","2048M");

    $file = env('PATH_REPORT_FILE')."/produtos_caixa.csv";
    $myfile = fopen($file, "w") or die("Unable to open file!");


    $html = "Plataforma;";
    $html .= "Data;";
    $html .= "Pedido;";
    $html .= "Area;";
    $html .= "Loja;";
    $html .= "Modelo;";
    $html .= "Codigo;";
    $html .= "Categoria;";
    $html .= "Preco;";
    $html .= "Semana;";
    $html .= "DataCaixa;";
    $html .= "Quantidade;";
    $html .= "Liberacao;";
    $html .= "Prioridade;";
    $html .= "Campanha;";
    $html .= "\n";

    fwrite($myfile, $html);

    $this->line('Escreveu cabecalho '.date('d-m-Y H:i:s'));

    $sql = "SELECT
    l.lv_confirm_picking_id as liberacao,
    b.*
    FROM vw_report_product_box_ppm b
    LEFT JOIN(
      SELECT
      lv.customer_id,
      lvo.week_number,
      max(lvo.lv_confirm_picking_id) as  lv_confirm_picking_id
      FROM
      lv_confirm_picking_orders lvo
      join lv_confirm_picking lv on lv.lv_confirm_picking_id = lvo.lv_confirm_picking_id
      where lvo.type_system = 'ppm'
      and lv.active = 1
      and lv.is_admin = 0
      and lvo.active = 1
      and lv.tipo_usuario = 1
      group by 1,2) l on l.week_number = b.semana AND b.customer_id = l.customer_id;";

      $results_ppm =  DB::select($sql);

      $this->line('Terminou query do PPM '.date('d-m-Y H:i:s'));

      $html = "";

      foreach ($results_ppm as $result) {

        //    $html = "";
        $html .= $result->plataforma.";";
        $html .= date("d/m/Y",strtotime($result->data)).";";
        $html .= $result->pedido.";";
        $html .= $result->area.";";
        $html .= $result->loja.";";
        $html .= $result->modelo.";";
        $html .= $result->brazil_code.";";
        $html .= $result->categoria.";";
        $html .= $result->preco.";";
        $html .= $result->semana.";";
        $html .= $result->date_box.";";
        $html .= $result->quantidade.";";
        $html .= $result->liberacao.";";
        $html .= '0'.";";
        $html .= ''.";";
        $html .= "\n";
      }

      fwrite($myfile, $html);

      $this->line('Escreveu PPM '.date('d-m-Y H:i:s'));


      $sql = "SELECT
      b.*,
      (SELECT max(lvo.lv_confirm_picking_id) lv_confirm_picking_id
      FROM
      lv_confirm_picking_orders lvo
      join lv_confirm_picking lv on lv.lv_confirm_picking_id = lvo.lv_confirm_picking_id
      where lvo.type_system = 'csm'
      and lv.active = 1
      and lv.is_admin = 0
      and lvo.active = 1
      and lvo.order_id = b.pedido limit 1) as liberacao,
      (select invoice_no FROM webstore.order WHERE order_id = b.pedido limit 1) as prioridade
      FROM vw_report_product_box_csm b
      WHERE b.quantidade > 0";

      $results_csm =  DB::select($sql);

      $this->line('Terminou query do CSM '.date('d-m-Y H:i:s'));

      $html = "";

      foreach ($results_csm as $result) {

        //    $html = "";
        $html .= $result->plataforma.";";
        $html .= date("d/m/Y",strtotime($result->data)).";";
        $html .= $result->pedido.";";
        $html .= $result->area.";";
        $html .= $result->loja.";";
        $html .= $result->modelo.";";
        $html .= $result->brazil_code.";";
        $html .= $result->categoria.";";
        $html .= $result->preco.";";
        $html .= $result->semana.";";
        $html .= $result->date_box.";";
        $html .= $result->quantidade.";";
        $html .= $result->liberacao.";";
        $html .= $result->prioridade.";";
        $html .= ''.";";
        $html .= "\n";
      }

      fwrite($myfile, $html);

      $this->line('Escreveu CSM '.date('d-m-Y H:i:s'));


      $sql = "SELECT
      b.*,
      (SELECT max(lvo.lv_confirm_picking_id) lv_confirm_picking_id
      FROM
      lv_confirm_picking_orders lvo
      join lv_confirm_picking lv on lv.lv_confirm_picking_id = lvo.lv_confirm_picking_id
      where lvo.type_system = 'lv'
      and lv.active = 1
      and lv.is_admin = 0
      and lvo.active = 1
      and lvo.order_id = b.pedido limit 1) as liberacao
      FROM vw_report_product_box_lojavirtual b
      WHERE b.quantidade > 0;";

      $results_lojavirtual =  DB::select($sql);

      $this->line('Terminou query do LV '.date('d-m-Y H:i:s'));

      $html = "";


      foreach ($results_lojavirtual as $result)
      {

        $campanha = '';
        if($result->is_campanha==1)
        {
          $campanha = 'S';
        }
        //    $html = "";
        $html .= $result->plataforma.";";
        $html .= date("d/m/Y",strtotime($result->data)).";";
        $html .= $result->pedido.";";
        $html .= $result->area.";";
        $html .= $result->loja.";";
        $html .= $result->modelo.";";
        $html .= $result->brazil_code.";";
        $html .= $result->categoria.";";
        $html .= $result->preco.";";
        $html .= $result->semana.";";
        $html .= $result->date_box.";";
        $html .= $result->quantidade.";";
        $html .= $result->liberacao.";";
        $html .= '0'.";";
        $html .= $campanha.";";
        $html .= "\n";

      }

      $this->line('Escreveu LV '.date('d-m-Y H:i:s'));

      fwrite($myfile, $html);

      //$txt = $html;

      fclose($myfile);

    }
  }
