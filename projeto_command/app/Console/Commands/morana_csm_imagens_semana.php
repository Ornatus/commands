<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class morana_csm_imagens_semana extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-csm-imagens-semana';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Faz os thumbnails dos produtos do bolsão csm';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    $this->line('Iniciando command');

    $sql = "SELECT * FROM vw_csm_bolsao_produtos; ";

    $products = DB::select($sql);

    foreach($products as $product)
    {
      $url = sprintf(env('HTTP_CREATE_IMAGE'),500,500,$product->image);
      $name = env('PATH_THUMB_CSM').'/view/image/CSM/50/'.$product->product_id.'.jpg';

      $this->line('Arquivo: '. $name );

      if(file_exists($name))
      {
        // unlink($name);
        continue;
      }

      $file_content = file_get_contents($url);

      if(strlen($file_content) < 100)
      {
        $this->line('Arquivo '.$name.' com problemas, foi foi possível fazer a thumb');
        continue;
      }

      file_put_contents($name, $file_content);

    }

  }




}
