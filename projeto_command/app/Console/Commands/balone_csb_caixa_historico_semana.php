<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class balone_csb_caixa_historico_semana extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:balone-csb-caixa-historico-semana';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Gera o relatório de itens na caixa com os ultimos pedidos de CSB';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    ini_set("memory_limit","2048M");

    $file = env('PATH_REPORT_FILE')."/morana_csb_caixa_historico_semana.csv";
    $myfile = fopen($file, "w") or die("Unable to open file!");

    $this->line('Coletando Customers - '.date('d-m-Y H:i:s'));

    $last_sunday =  date('Y-m-d',strtotime('last sunday'));

    $sql = "  SELECT
    c.customer_id,
    c.brazil_store_id,
    c.brazil_store_name,
    c.rede,
    u.firstname as supervisor
    from balone_customer as c
    LEFT JOIN user as u ON u.user_id = c.supervisor_id
    WHERE c.ppe_access = 1
    ORDER BY c.customer_id ASC ";

    $customers =  DB::select($sql);

    $semanas = $this->getUltimas4SemanasCsb();

    $semanas_lv = $this->getWeeksFilter($semanas);

    foreach ($semanas as $semana)
    {
      foreach ($semanas_lv as $semana2)
      {
        if($semana->news_week_id == $semana2->news_week_id)
        {
          $semana->semana = $semana2->semana;
          $semana->ano = $semana2->ano;
        }
      }
    }

    sort($semanas);

    $caixa = array();

    $flag = 1;

    foreach($customers as $customer)
    {
      $this->line('Itens na caixa de  - '.$customer->brazil_store_name.' '.date('d-m-Y H:i:s'));

      $sql = "Call getVendasPcUltSemanas('0001','".$customer->brazil_store_id."','".$last_sunday."',6);";

      try {
        if($flag == 1)
        {
          if($customer->rede == 2 || $customer->rede == 3 )
          {
            $return = $this->getAverageSalesLb($customer->customer_id);
          }
          else
          {
              $return = DB::connection('mysql3')->select($sql);
          }

        }

      } catch (\Exception $e) {
        $this->line('Erro ao consultar DSB  - '.$e.' '.date('d-m-Y H:i:s'));
        $flag = 0;
      }

      $caixa[$customer->customer_id]['media'] = $this->getAverage($return);
      $caixa[$customer->customer_id]['area']=$customer->brazil_store_id;
      $caixa[$customer->customer_id]['loja']=$customer->brazil_store_name;
      $caixa[$customer->customer_id]['supervisor']=$customer->supervisor;

      $itens_caixa = $this->getItensCaixaCustomer($customer->customer_id);

      if(count( $itens_caixa ) > 0)
      {
        $caixa[$customer->customer_id]['caixa']=$itens_caixa[0]->qtd;
      }
      else
      {
        $caixa[$customer->customer_id]['caixa']=0;
      }

      foreach($semanas as $semana)
      {

        $caixa[$customer->customer_id][$semana->news_week_id] = $this->getQuantidadeSemana($customer->customer_id, $semana->news_week_id);
        $caixa[$customer->customer_id][$semana->semana] = $this->getQuantidadeSemanaLojaVirtual($customer->customer_id, $semana->semana , $semana->ano);
        $caixa[$customer->customer_id]['liberacao_'.$semana->semana] = $this->getQtdeLiberacao($customer->customer_id, $semana->semana , $semana->ano);
      }

    }

    $this->line('Iniciando Excel - '.date('d-m-Y H:i:s'));

    $html = "Area;";
    $html .= "Loja;";
    $html .= "Consultor;";
    $html .= "Produtos na Caixa;";
    $html .= "Media Pecas Vendidas por Semana;";

    foreach ($semanas as $semana) {
      $html .= "CSB {$semana->news_week_id};";
      $html .= "LOJA VIRTUAL {$semana->news_week_id};";
      $html .= "PECAS LIBERADAS {$semana->news_week_id};";
    }

    $html .= "\n";

    fwrite($myfile, $html);

    $html = "";

    foreach ($caixa as $cx)
    {

      $html .= $cx['area'].";";
      $html .= htmlspecialchars_decode(utf8_decode($cx['loja'])).";";
      $html .= htmlspecialchars_decode(utf8_decode($cx['supervisor'])).";";
      $html .= $cx['caixa'].";";
      $html .= $cx['media'].";";

      foreach($semanas as $semana)
      {
        $html .= $cx[$semana->news_week_id].";";
        $html .= $cx[$semana->semana].";";
        $html .= $cx['liberacao_'.$semana->semana].";";
      }

      $html .= "\n";

    }

    fwrite($myfile, $html);

    fclose($myfile);

  }

  public function getItensCaixaCustomer($customer_id)
  {
    $sql = "SELECT customer_id, SUM(quantidade) AS qtd
    FROM (
      SELECT
      'CSB' AS plataforma,
      o.customer_id,
      date(o.date_added) as date_added,
      o.order_id AS pedido,
      p.product_id,
      p.model,
      p.brazil_code, SUBSTRING(p.model,8,3) AS categoria,
      fp.suggested_price,
      pd.brazil_description AS descricao,
      ps.name as product_name,
      s1.subcategory_name AS sub1_name,
      s1.subcategory_id,
      s2.subcategory_name AS sub2_name,
      s2.subcategory_id subcategory2_id,
      se.segment_name AS segmento,
      se.segment_id,
      (COALESCE(op.quantity,0) + COALESCE(op.extra_quantity,0)) AS quantidade
      FROM balone.order_product op
      JOIN balone.order o ON o.order_id = op.order_id
      JOIN product p ON p.product_id = op.product_id
      JOIN product_fixed_price_brazil fp ON fp.product_id = p.product_id
      JOIN balone_customer c ON c.customer_id = o.customer_id
      JOIN product_description pd ON pd.product_id = p.product_id AND pd.language_id = 3
      LEFT JOIN product_sub_category ps ON ps.product_id = p.product_id
      LEFT JOIN subcategory s1 ON s1.subcategory_id = ps.subcategory_id
      LEFT JOIN subcategory s2 ON s2.subcategory_id = ps.sub_category2_id
      LEFT JOIN segment se ON se.segment_id = ps.segment_id
      WHERE o.customer_id = '".$customer_id."' AND  o.purchase_type IN ('A','N') AND o.order_status_id = 3 AND (op.quantity>0 OR op.extra_quantity>0) UNION
      SELECT
      'CSB' AS plataforma,
      o.customer_id,
      date(o.date_added) as date_added,
      o.order_id AS pedido,
      p.product_id,
      p.model,
      p.brazil_code, SUBSTRING(p.model,8,3) AS categoria,
      fp.suggested_price,
      pd.brazil_description AS descricao,
      ps.name as product_name,
      s1.subcategory_name AS sub1_name,
      s1.subcategory_id,
      s2.subcategory_name AS sub2_name,
      s2.subcategory_id subcategory2_id,
      se.segment_name AS segmento,
      se.segment_id,
      COALESCE(op.quantity_partial,0) AS quantidade
      FROM balone.order_product op
      JOIN balone.order o ON o.order_id = op.order_id
      JOIN product p ON p.product_id = op.product_id
      JOIN product_fixed_price_brazil fp ON fp.product_id = p.product_id
      JOIN balone_customer c ON c.customer_id = o.customer_id
      JOIN product_description pd ON pd.product_id = p.product_id AND pd.language_id = 3
      LEFT JOIN product_sub_category ps ON ps.product_id = p.product_id
      LEFT JOIN subcategory s1 ON s1.subcategory_id = ps.subcategory_id
      LEFT JOIN subcategory s2 ON s2.subcategory_id = ps.sub_category2_id
      LEFT JOIN segment se ON se.segment_id = ps.segment_id
      WHERE  o.customer_id = '".$customer_id."' AND o.purchase_type IN ('R') AND o.order_status_id = 3 AND (op.quantity_partial>0) UNION
      SELECT
      'LVB' AS plataforma,
      o.customer_id,
      date(o.date_added) as date_added,
      o.lv_order_id AS pedido,
      p.product_id,
      p.model,
      p.brazil_code, SUBSTRING(p.model,8,3) AS categoria,
      fp.suggested_price,
      pd.brazil_description AS descricao,
      ps.name as product_name,
      s1.subcategory_name AS sub1_name,
      s1.subcategory_id,
      s2.subcategory_name AS sub2_name,
      s2.subcategory_id subcategory2_id,
      se.segment_name AS segmento,
      se.segment_id,
      COALESCE(op.quantity_partial,0) AS quantidade
      FROM balone.lv_order_product_balone op
      JOIN balone.lv_order_balone o ON o.lv_order_id = op.lv_order_id
      JOIN product p ON p.product_id = op.product_id
      JOIN product_fixed_price_brazil fp ON fp.product_id = p.product_id
      JOIN balone_customer c ON c.customer_id = o.customer_id
      JOIN product_description pd ON pd.product_id = p.product_id AND pd.language_id = 3
      LEFT JOIN product_sub_category ps ON ps.product_id = p.product_id
      LEFT JOIN subcategory s1 ON s1.subcategory_id = ps.subcategory_id
      LEFT JOIN subcategory s2 ON s2.subcategory_id = ps.sub_category2_id
      LEFT JOIN segment se ON se.segment_id = ps.segment_id
      WHERE  o.customer_id = '".$customer_id."' AND o.status_id = 3 AND (op.quantity_partial>0)

    ) AS ped GROUP BY 1";

    $results =  DB::select($sql);

    return $results;
  }

  public function getUltimas4SemanasCsb()
  {
    $sql = "SELECT news_week_id FROM balone_news_week
    WHERE confirmed = 'S'
    AND date_end < current_date
    ORDER BY 1 DESC LIMIT 4;";

    $results =  DB::select($sql);

    return $results;
  }

  public function getQuantidadeSemana($customer_id, $news_week_id)
  {
    $sql = "SELECT news_week_id, SUM(qtd) as qtd
    FROM
    (
      SELECT o.news_week_id,
      sum(COALESCE(quantity,0) + COALESCE(extra_quantity,0)) AS qtd
      FROM balone.order_product op
      JOIN balone.order o ON o.order_id = op.order_id
      WHERE o.purchase_type IN ('A','N')
      AND o.news_week_id = '".$news_week_id."'
      AND o.customer_id = '".$customer_id."'
      GROUP BY 1
      UNION
      SELECT o.news_week_id,
      sum(COALESCE(quantity_partial,0)) AS qtd
      FROM balone.order_product op
      JOIN balone.order o ON o.order_id = op.order_id
      WHERE o.purchase_type = 'R'
      AND o.news_week_id = '".$news_week_id."'
      AND o.customer_id = '".$customer_id."'
      GROUP BY 1 ) AS t
      GROUP BY 1";

      $results =  DB::select($sql);

      $qtd = 0;

      if(count($results) > 0)
      {
        $qtd = $results[0]->qtd;
      }

      return $qtd;

    }

    public function getWeeksFilter($data)
    {
      $sql = "SELECT
      news_week_id,
      WEEK(date_end,3) as semana,
      YEAR(date_end) as ano
      FROM balone_news_week
      WHERE confirmed = 'S'
      AND date_end < current_date
      AND news_week_id >= '".$data[3]->news_week_id."'
      AND news_week_id <= '".$data[0]->news_week_id."'
      ORDER BY 1 DESC ";

      $results =  DB::select($sql);

      return $results;
    }

    public function getQtdeLiberacao($customer_id, $semana, $ano)
    {
      $sql = " SELECT customer_id,SUM(pecas) as qtd FROM (SELECT
        lvc.lv_confirm_picking_id,
        lvcpo.type_system,
        lvcpo.order_id,
        lvc.customer_id,
        CASE WHEN lvcpo.type_system = 'lvb'
        THEN
        (SELECT SUM(lvop.quantity) as qtd_lv
        from balone.lv_order_balone as lvo
        JOIN balone.lv_order_product_balone as lvop ON lvop.lv_order_id = lvo.lv_order_id
        WHERE lvo.lv_order_id = lvcpo.order_id
        AND lvo.ativo = 1)
        WHEN  lvcpo.type_system = 'csb'
        THEN
        (SELECT
          CASE
          WHEN o.purchase_type = 'R' THEN SUM(op.quantity_partial)
          ELSE SUM(COALESCE(quantity, 0) + COALESCE(extra_quantity, 0))
          END AS qtd_pedida
          FROM
          balone.order_product op
          JOIN
          balone.order o ON o.order_id = op.order_id
          WHERE
          op.order_id = lvcpo.order_id
          AND o.customer_id = lvc.customer_id
          AND 0 < CASE
          WHEN o.purchase_type = 'R' THEN op.quantity_partial
          ELSE COALESCE(op.quantity, 0) + COALESCE(op.extra_quantity, 0)
          END)
          ELSE 0 END as pecas

          FROM
          lv_confirm_picking AS lvc
          JOIN
          lv_confirm_picking_orders AS lvcpo ON lvcpo.lv_confirm_picking_id = lvc.lv_confirm_picking_id
          WHERE
          lvc.active = 1
          AND YEAR(lvc.data_add) = '".$ano."'
          AND WEEK(lvc.data_add, 3) = '".$semana."'
          AND lvc.tipo_usuario = '3'
          AND lvc.customer_id = '".$customer_id."'  ) as t;
          ";


          $results =  DB::select($sql);

          $qtd = 0;

          if(count($results) > 0 && $results[0]->qtd != '' && $results[0]->qtd > 0)
          {
            $qtd = $results[0]->qtd;
          }

          return $qtd;

        }

        function getAverage($array)
        {
          $semanas =count($array);

          $total = 0;
          foreach ($array as $item)
          {
            $total += $item->PcVen;
          };

          return (count($array) > 0 ) ? number_format($total/count($array),0) : 0 ;
        }

        public function getQuantidadeSemanaLojaVirtual($customer_id, $semana, $ano)
        {
          $sql = " SELECT SUM( qtd )as qtd FROM(
            SELECT
            lvo.lv_order_id,
            CASE
            WHEN lvo.status_id IN (1,2)
            THEN SUM(lvop.quantity)
            WHEN lvo.status_id IN (3,8)
            THEN SUM(lvop.quantity_partial)
            END as qtd
            FROM balone.lv_order_balone as lvo
            JOIN balone.lv_order_product_balone as lvop  ON lvop.lv_order_id = lvo.lv_order_id
            WHERE lvo.customer_id = '".$customer_id."'
            AND lvo.type_user_id = 1
            AND lvo.ativo = 1
            AND YEAR(lvo.date_added) = '".$ano."'
            AND WEEK(lvo.date_added,3) = '".$semana."'
            AND lvo.status_id IN (1,2,3,8)
            group by 1 ) as total
            ";


            $results =  DB::select($sql);

            $qtd = 0;

            if(count($results) > 0 && $results[0]->qtd != '' && $results[0]->qtd > 0)
            {
              $qtd = $results[0]->qtd;
            }

            return $qtd;

          }

          public function getAverageSalesLb($customer_id)
          {
            $sql = "SELECT
                      WEEK(v.data_saida) as semana,
                      SUM(v.quantidade_saida) as PcVen
                    FROM balone.lb_vendas v
                      JOIN balone.customer_linx  c on c.loja = v.loja
                      WHERE c.customer_id = '".$customer_id."'
                      GROUP BY 1
                      ORDER BY 1 DESC LIMIT 6;";


              $results =  DB::select($sql);

              return $results;

            }

      }
