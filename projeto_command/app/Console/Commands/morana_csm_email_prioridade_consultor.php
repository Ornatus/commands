<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class morana_csm_email_prioridade_consultor extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-csm-email-prioridade-consultor';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Envia email para os consultores com pedidos perto da data limite de midia';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {

    $sql = "SELECT
    news_week_id,
    priority,
    date_start
    from news_week_priority
    where date_start >= current_date() limit 1";

    $info = DB::select($sql);

    if(count($info) < 1)
    {
      $this->line('Nenhum prioridade encontrada - '.date('d-m-Y H:i:s'));
      return false;
    }

    $sql = "SELECT
    distinct u.user_id,
    u.email,
    u.username
    FROM user as u
    JOIN customer as c ON c.supervisor_id = u.user_id
    WHERE ppe_access = 1";

    $supervidores = DB::select($sql);

    foreach ($supervidores as $supervisor)
    {
      $this->line('Coletando pedidos das lojas relacionadas ao supervisor(a): '.$supervisor->username.' - '.date('d-m-Y H:i:s'));

      $sql = "SELECT
      o.order_id,
      c.customer_id,
      c.brazil_store_name,
      tot.qtd_products,
      tot.total_suggested_price,
      ROUND(tot.total, 2) AS total,
      o.date_added,
      o.invoice_no,
      r.descricao,
      r.dias_frete,
      r.regional_id,
      u.firstname,
      lvp.lv_confirm_picking_id,
      DATE(lvp.data_add) AS data_add,
      DATE_FORMAT(nwp.date_start, '%d/%m/%Y') AS date_start_formated,
      DATE_FORMAT(nwp.date_end, '%d/%m/%Y') AS date_end_formated,
      nwp.date_start,
      nwp.date_end,
      o.order_status_id
      FROM
      morana2.customer c
      JOIN
      webstore.order o ON o.customer_id = c.customer_id
      JOIN
      webstore.order_product op ON op.order_id = o.order_id
      LEFT JOIN
      morana2.user AS u ON u.user_id = c.supervisor_id
      LEFT JOIN
      morana2.news_week_priority AS nwp ON nwp.news_week_id = o.news_week_id
      AND nwp.priority = o.invoice_no
      AND nwp.active = 1
      LEFT JOIN
      morana2.regional AS r ON r.regional_id = c.regional_id
      LEFT JOIN
      morana2.lv_confirm_picking_orders AS lvpo ON lvpo.order_id = o.order_id
      AND lvpo.type_system = 'csm'
      AND lvpo.active = 1
      LEFT JOIN
      morana2.lv_confirm_picking AS lvp ON lvp.lv_confirm_picking_id = lvpo.lv_confirm_picking_id
      AND lvp.active = 1
      LEFT JOIN
      (SELECT
        o.order_id,
        c.customer_id,
        c.brazil_store_name,
        COALESCE(SUM(quantity), 0) + COALESCE(SUM(extra_quantity), 0) AS qtd_products,
        SUM((((quantity) + (extra_quantity)) * fp.suggested_price)) total_suggested_price,
        (fp.fixed_price + ((fp.suggested_price / 100) * 5)) * COALESCE(SUM(quantity), 0) + COALESCE(SUM(extra_quantity), 0) AS total,
        o.date_added
        FROM
        webstore.order_product op
        JOIN webstore.order o ON o.order_id = op.order_id
        JOIN customer c ON c.customer_id = o.customer_id
        JOIN product_fixed_price_brazil fp ON fp.product_id = op.product_id
        WHERE
        o.news_week_id = '".$info[0]->news_week_id."'
        AND o.purchase_type IN ('N' , 'A', 'Z')
        GROUP BY 1 , 2 , 3) AS tot ON tot.order_id = o.order_id
        AND c.customer_id = tot.customer_id
        WHERE
        o.news_week_id = '".$info[0]->news_week_id."'
        AND o.purchase_type IN ('N' , 'A', 'Z')
        AND c.supervisor_id = '".$supervisor->user_id."'
        AND o.invoice_no = '".$info[0]->priority."'
        AND o.order_status_id = '3'
        GROUP BY 1 , 2 , 3
        ORDER BY qtd_products DESC ";

        $orders = DB::select($sql);

        $this->line('Verificando se existem pedidos desse consultor(a) - '.date('d-m-Y H:i:s'));

        if(count($orders) > 0)
        {
          $data = array();
          $data['emails'] = array();

          $data['orders'] = $orders;
          $data['supervisor'] = $supervisor;
          $data['info_news_week_priority'] = $info;

          $view = 'mail/email_prioridade_consultor';

          if( isset($supervisor->email) && strlen($supervisor->email) > 3 )
          {
              $data['emails'][] = $supervisor->email;
          }

          $data['emails'][] = 'vaneza.denofrio@grupoornatus.com';
          $data['emails'][] = 'sistema03@grupoornatus.com';
          $data['emails'][] = 'devmorana@gmail.com';

          if(count($data['emails']) > 0)
          {
            $this->line('Enviando Email - '.date('d-m-Y H:i:s'));

            Mail::send($view, $data, function($message) use ($data) {
              $subject ='MORANA - Liberação de Prioridades CSM';
              $message->to($data['emails'],'')->subject( (string)$subject );
              $message->from('dev@grupoornatus.com','[MORANA]');
            });

          }

        }else
        {
          $this->line('Não existem pedidos deste consultor(a) - '.date('d-m-Y H:i:s'));
        }

      }

    }
  }
