<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class morana_itens_caixa_ecommerce extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-itens-caixa-ecommerce';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Gera a lista de produtos na caixa do e-commerce no formato para importar';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {

    ini_set("memory_limit","2048M");

    $file = env('PATH_REPORT_FILE')."/produtos_caixa_ecommerce.csv";
    $myfile = fopen($file, "w") or die("Unable to open file!");

    $cor = array();
    $cor['BK']['descricao'] = 'GRAFITE';
    $cor['GD']['descricao'] = 'DOURADO';
    $cor['OR']['descricao'] = 'PRATA';

    $categorias = array();
    $categorias['ANK']['descricao'] = 'TORNOZELEIRA';
    $categorias['ERR']['descricao'] = 'BRINCO';
    $categorias['RNG']['descricao'] = 'ANEL';
    $categorias['BRC']['descricao'] = 'PULSEIRA';
    $categorias['SET']['descricao'] = 'CONJUNTO';
    $categorias['HAR']['descricao'] = 'CABELO';
    $categorias['NCL']['descricao'] = 'COLAR';
    $categorias['MAT']['descricao'] = 'MATERIAL DE APOIO';
    $categorias['PDT']['descricao'] = 'PINGENTE';
    $categorias['BAG']['descricao'] = 'BOLSA';

    $categorias['ANK']['ncm'] = '71179000';
    $categorias['ERR']['ncm'] = '71179000';
    $categorias['RNG']['ncm'] = '71179000';
    $categorias['BRC']['ncm'] = '71179000';
    $categorias['SET']['ncm'] = '71179000';
    $categorias['HAR']['ncm'] = '96151900';
    $categorias['NCL']['ncm'] = '71179000';
    $categorias['MAT']['ncm'] = '';
    $categorias['PDT']['ncm'] = '71179000';
    $categorias['BAG']['ncm'] = '42022210';

    $sql = "SELECT * FROM segment WHERE active = 1 ";
    $segmentos =  DB::select($sql);

    $sql = "SELECT * FROM subcategory WHERE active = 1 ";
    $subcategorias =  DB::select($sql);

    $html = "PLATAFORMA;";
    $html .= "PEDIDO;";
    $html .= "DATA PEDIDO;";
    $html .= "REFERENCIA (SKU);";
    $html .= "SEM GETIN;";
    $html .= "NOME PRODUTO;";
    $html .= "COR;";
    $html .= "TAMANHO;";
    $html .= "MARCA;";
    $html .= "PRECO VAREJO;";
    $html .= "PRECO CUSTO;";
    $html .= "CATEGORIA;";
    $html .= "SUB-CATEGORIA;";
    $html .= "SUB-CATEGORIA SIMILAR;";
    $html .= "SEGMENTACAO;";
    $html .= "CATEGORIA FISCAL;";
    $html .= "% DE BASE REDUZIDA;";
    $html .= "ALIQUOTA ICMS;";
    $html .= "NCM;";
    $html .= "CST;";
    $html .= "COD. BARRAS (EAN);";
    $html .= "QUANTIDADE;";
    $html .= "DESCRICAO;";
    $html .= "COMPOSICAO DO PRODUTO;";
    $html .= "ALTURA;";
    $html .= "LARGURA;";
    $html .= "PROFUNDIDADE;";
    $html .= "PESO;";
    $html .= "OBSERVACOES;";
    $html .= "\n";

    fwrite($myfile, $html);

    $this->line('Escreveu cabecalho '.date('d-m-Y H:i:s'));

    //<!-- LV -->

    $sql = "SELECT
    SUBSTRING(b.modelo, 18, 2) as cor,
    b.*,
    date(b.data) as data_pedido,
    CASE
    WHEN SUBSTRING(b.modelo, 8, 3) NOT IN ('ANK' , 'ERR', 'RNG', 'BRC', 'SET', 'ANK', 'HAR', 'NCL', 'MAT', 'PDT', 'BAG') THEN 'OUTROS'
    ELSE SUBSTRING(b.modelo, 8, 3)
    END AS categoria,
    pd.brazil_description,
    pf.suggested_price,
    ps.segment_id,
    ps.subcategory_id,
    ps.sub_category2_id,
    ps.name,
    sub.subcategory_name sub_name_1,
    sub2.subcategory_name as sub_name_2,
    seg.segment_name as seg_name
    FROM
    vw_report_product_box_lojavirtual b
    JOIN
    customer AS c ON c.customer_id = b.customer_id
    JOIN
    product AS p ON p.product_id = b.product_id
    LEFT JOIN   product_description AS pd ON pd.product_id = p.product_id AND pd.language_id = 3
    LEFT JOIN   product_fixed_price_brazil AS pf ON pf.product_id = b.product_id
    LEFT JOIN   product_sub_category as ps ON ps.product_id = b.product_id AND ps.active = 1
    LEFT JOIN subcategory sub on sub.subcategory_id = ps.subcategory_id
    LEFT JOIN subcategory sub2 on sub2.subcategory_id = ps.sub_category2_id
    LEFT JOIN segment seg ON seg.segment_id = ps.segment_id
    WHERE
    c.brazil_store_id = 'ECO' AND quantidade > 0";

    $results_lv =  DB::select($sql);

    $this->line('Terminou query do LV '.date('d-m-Y H:i:s'));

    $html = "";

    foreach ($results_lv as $result) {

      $html .= $result->plataforma.";";
      $html .= $result->pedido.";";
      $html .= $result->data_pedido.";";
      $html .= $result->brazil_code.";";
      $html .= "SEM GTIN;";
      $html .= htmlspecialchars_decode(utf8_decode($result->name)).";";

      if(isset($cor[$result->cor])){
        $html .= $cor[$result->cor]['descricao'].";";
      }else{
        $html .= ";";
      }

      $html .= "UNICO;";
      $html .= ";";
      $html .= str_replace(".",",",$result->suggested_price).";";
      $html .= ";";

      $ncm = "";

      if(isset($categorias[$result->categoria])){
        $html .= $categorias[$result->categoria]['descricao'].";";
        $ncm = $categorias[$result->categoria]['ncm'];
      }else{
        $html .= "OUTROS;";
      }

  /*    $subcategoria_row = "";

      foreach ($subcategorias as $subcategoria) {
        if($result->subcategory_id == $subcategoria->subcategory_id){
          $subcategoria_row =$subcategoria->subcategory_name;
        }
      }*/


    //  $html .= preg_replace('/\s+/', ' ', htmlspecialchars_decode(utf8_decode($subcategoria_row))).";";
    $html .= htmlspecialchars_decode(utf8_decode($result->sub_name_1)).";";

      // $html .= preg_replace('/\s+/', ' ', htmlspecialchars_decode(utf8_decode($subcategoria_row2))).";";

      $html .= htmlspecialchars_decode(utf8_decode($result->sub_name_2)).";";


      $html .= htmlspecialchars_decode(utf8_decode($result->seg_name)).";";

    //  $html .= preg_replace('/\s+/', ' ', htmlspecialchars_decode(utf8_decode($segmento_row))).";";

      $html .= "morana;";
      $html .= "synapcom;";
      $html .= "synapcom;";
      $html .= $ncm.";";
      $html .= "200;";
      $html .= "SEM GTIN;";
      $html .= $result->quantidade.";";
      $html .= preg_replace('/\s+/', ' ', htmlspecialchars_decode(utf8_decode($result->brazil_description))).";";
      $html .= ";";
      $html .= "20cm;";
      $html .= "14cm;";
      $html .= "6cm;";
      $html .= "100g;";
      $html .= ";";

      $html .= "\n";

    }

    fwrite($myfile, $html);

    $html = "";

    $this->line('Escreveu LV '.date('d-m-Y H:i:s'));

    $sql = "SELECT
    SUBSTRING(b.modelo, 18, 2) as cor,
    b.*,
    date(b.data) as data_pedido,
    CASE
    WHEN SUBSTRING(b.modelo, 8, 3) NOT IN ('ANK' , 'ERR', 'RNG', 'BRC', 'SET', 'ANK', 'HAR', 'NCL', 'MAT', 'PDT', 'BAG') THEN 'OUTROS'
    ELSE SUBSTRING(b.modelo, 8, 3)
    END AS categoria,
    pd.brazil_description,
    pf.suggested_price,
    ps.segment_id,
    ps.subcategory_id,
    ps.sub_category2_id,
    ps.name,
    sub.subcategory_name sub_name_1,
    sub2.subcategory_name as sub_name_2,
    seg.segment_name as seg_name
    FROM vw_report_product_box_ppm b
    LEFT JOIN(
      SELECT
      lv.customer_id,
      lvo.week_number,
      max(lvo.lv_confirm_picking_id) as  lv_confirm_picking_id
      FROM
      lv_confirm_picking_orders lvo
      join lv_confirm_picking lv on lv.lv_confirm_picking_id = lvo.lv_confirm_picking_id
      where lvo.type_system = 'ppm'
      and lv.active = 1
      and lv.is_admin = 0
      and lvo.active = 1
      and lv.tipo_usuario = 1
      group by 1,2) l on l.week_number = b.semana AND b.customer_id = l.customer_id
      JOIN
      customer AS c ON c.customer_id = b.customer_id
      JOIN
      product AS p ON p.product_id = b.product_id
      LEFT JOIN
      product_description AS pd ON pd.product_id = p.product_id AND pd.language_id = 3
      LEFT JOIN
      product_fixed_price_brazil AS pf ON pf.product_id = b.product_id
      LEFT JOIN
      product_sub_category as ps ON ps.product_id = b.product_id AND ps.active = 1
      LEFT JOIN subcategory sub on sub.subcategory_id = ps.subcategory_id
      LEFT JOIN subcategory sub2 on sub2.subcategory_id = ps.sub_category2_id
      LEFT JOIN segment seg ON seg.segment_id = ps.segment_id
      WHERE
      c.brazil_store_id = 'ECO' AND quantidade > 0";

      $results_ppm =  DB::select($sql);

      foreach ($results_ppm as $result) {

        $html .= $result->plataforma.";";
        $html .= $result->semana.";";
        $html .= $result->data_pedido.";";
        $html .= $result->brazil_code.";";
        $html .= "SEM GTIN;";
        $html .= htmlspecialchars_decode(utf8_decode($result->name)).";";

        if(isset($cor[$result->cor])){
          $html .= $cor[$result->cor]['descricao'].";";
        }else{
          $html .= ";";
        }

        $html .= "UNICO;";
        $html .= ";";
        $html .= str_replace(".",",",$result->suggested_price).";";
        $html .= ";";

        $ncm = '';

        if(isset($categorias[$result->categoria])){
          $html .= $categorias[$result->categoria]['descricao'].";";
          $ncm = $categorias[$result->categoria]['ncm'];
        }else{
          $html .= "OUTROS;";
        }

        /*    $subcategoria_row = "";

            foreach ($subcategorias as $subcategoria) {
              if($result->subcategory_id == $subcategoria->subcategory_id){
                $subcategoria_row =$subcategoria->subcategory_name;
              }
            }*/


          //  $html .= preg_replace('/\s+/', ' ', htmlspecialchars_decode(utf8_decode($subcategoria_row))).";";
          $html .= htmlspecialchars_decode(utf8_decode($result->sub_name_1)).";";

            // $html .= preg_replace('/\s+/', ' ', htmlspecialchars_decode(utf8_decode($subcategoria_row2))).";";

            $html .= htmlspecialchars_decode(utf8_decode($result->sub_name_2)).";";


            $html .= htmlspecialchars_decode(utf8_decode($result->seg_name)).";";

          //  $html .= preg_replace('/\s+/', ' ', htmlspecialchars_decode(utf8_decode($segmento_row))).";";

        $html .= "morana;";
        $html .= "synapcom;";
        $html .= "synapcom;";
        $html .= $ncm.";";
        $html .= "200;";
        $html .= "SEM GTIN;";
        $html .= $result->quantidade.";";
        $html .= preg_replace('/\s+/', ' ', htmlspecialchars_decode(utf8_decode($result->brazil_description))).";";
        $html .= ";";
        $html .= "20cm;";
        $html .= "14cm;";
        $html .= "6cm;";
        $html .= "100g;";
        $html .= ";";

        $html .= "\n";

      }

      fwrite($myfile, $html);

      $html = "";

      $this->line('Escreveu PPM '.date('d-m-Y H:i:s'));

      $sql = "SELECT
      SUBSTRING(b.modelo, 18, 2) as cor,
      b.*,
      date(b.data) as data_pedido,
      CASE
      WHEN SUBSTRING(b.modelo, 8, 3) NOT IN ('ANK' , 'ERR', 'RNG', 'BRC', 'SET', 'ANK', 'HAR', 'NCL', 'MAT', 'PDT', 'BAG') THEN 'OUTROS'
      ELSE SUBSTRING(b.modelo, 8, 3)
      END AS categoria,
      pd.brazil_description,
      pf.suggested_price,
      ps.segment_id,
      ps.subcategory_id,
      ps.sub_category2_id,
      (SELECT max(lvo.lv_confirm_picking_id) lv_confirm_picking_id
      FROM
      lv_confirm_picking_orders lvo
      join lv_confirm_picking lv on lv.lv_confirm_picking_id = lvo.lv_confirm_picking_id
      where lvo.type_system = 'csm'
      and lv.active = 1
      and lv.is_admin = 0
      and lvo.active = 1
      and lvo.order_id = b.pedido limit 1) as liberacao,
      ps.name,
      sub.subcategory_name sub_name_1,
      sub2.subcategory_name as sub_name_2,
      seg.segment_name as seg_name
      FROM vw_report_product_box_csm b
      JOIN
      customer AS c ON c.customer_id = b.customer_id
      JOIN
      product AS p ON p.product_id = b.product_id
      LEFT JOIN
      product_description AS pd ON pd.product_id = p.product_id AND pd.language_id = 3
      LEFT JOIN
      product_fixed_price_brazil AS pf ON pf.product_id = b.product_id
      LEFT JOIN
      product_sub_category as ps ON ps.product_id = b.product_id AND ps.active = 1
      LEFT JOIN subcategory sub on sub.subcategory_id = ps.subcategory_id
      LEFT JOIN subcategory sub2 on sub2.subcategory_id = ps.sub_category2_id
      LEFT JOIN segment seg ON seg.segment_id = ps.segment_id
      WHERE
      c.brazil_store_id = 'ECO' AND quantidade > 0";

      $results_csm =  DB::select($sql);

      foreach ($results_csm as $result) {

        $html .= $result->plataforma.";";
        $html .= $result->pedido.";";
        $html .= $result->data_pedido.";";
        $html .= $result->brazil_code.";";
        $html .= "SEM GTIN;";
        $html .= htmlspecialchars_decode(utf8_decode($result->name)).";";

        if(isset($cor[$result->cor])){
          $html .= $cor[$result->cor]['descricao'].";";
        }else{
          $html .= ";";
        }

        $html .= "UNICO;";
        $html .= ";";
        $html .= str_replace(".",",",$result->suggested_price).";";
        $html .= ";";

        $ncm = '';

        if(isset($categorias[$result->categoria])){
          $html .= $categorias[$result->categoria]['descricao'].";";
          $ncm = $categorias[$result->categoria]['ncm'];
        }else{
          $html .= "OUTROS;";
        }

        /*    $subcategoria_row = "";

            foreach ($subcategorias as $subcategoria) {
              if($result->subcategory_id == $subcategoria->subcategory_id){
                $subcategoria_row =$subcategoria->subcategory_name;
              }
            }*/


          //  $html .= preg_replace('/\s+/', ' ', htmlspecialchars_decode(utf8_decode($subcategoria_row))).";";
          $html .= htmlspecialchars_decode(utf8_decode($result->sub_name_1)).";";

            // $html .= preg_replace('/\s+/', ' ', htmlspecialchars_decode(utf8_decode($subcategoria_row2))).";";

            $html .= htmlspecialchars_decode(utf8_decode($result->sub_name_2)).";";


            $html .= htmlspecialchars_decode(utf8_decode($result->seg_name)).";";

          //  $html .= preg_replace('/\s+/', ' ', htmlspecialchars_decode(utf8_decode($segmento_row))).";";

        $html .= "morana;";
        $html .= "synapcom;";
        $html .= "synapcom;";
        $html .= $ncm.";";
        $html .= "200;";
        $html .= "SEM GTIN;";
        $html .= $result->quantidade.";";
        $html .= preg_replace('/\s+/', ' ', htmlspecialchars_decode(utf8_decode($result->brazil_description))).";";
        $html .= ";";
        $html .= "20cm;";
        $html .= "14cm;";
        $html .= "6cm;";
        $html .= "100g;";
        $html .= ";";

        $html .= "\n";

      }

      fwrite($myfile, $html);

      $html = "";

      $this->line('Escreveu CSM '.date('d-m-Y H:i:s'));


      fclose($myfile);

    }
  }
