<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class balone_lb_importacao_vendas extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:balone-lb-importacao-vendas';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Importa excel com os dados das vendas de ponta das Love Brands';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    header('Content-Type: text/html; charset=UTF-8');

    $lines = 0;

    $sql = "SELECT
    *
    FROM balone.lb_importacao_vendas
    WHERE import = 0
    LIMIT 1;";

    $file_info = DB::select($sql);

    $idFile = $file_info[0]->lb_importacao_venda_id;
    $fileName = $file_info[0]->nome_arquivo;

    $file = env('FILE_IMPORT').$fileName;

    $ponteiro = fopen($file, "r");

    while (!feof($ponteiro))
    {

      $row = fgets($ponteiro, 4096);

      if (!empty(trim($row)))
      {
        if($lines > 7)
        {

          $data = array();

          $this->line('Linha: '.$lines.' || '.date('d-m-Y H:i:s'));
          
          $collums = explode(";", $row);


          if(count($collums) != 17)
          {
            $this->line('Arquivo com colunas diferente do padrao (17) || '.date('d-m-Y H:i:s'));

            $sql = "UPDATE balone.lb_importacao_vendas
            SET
            observacao = 'Arquivo com colunas diferente do padrão (17)'
            WHERE lb_importacao_venda_id = '".$idFile."' ";

            DB::update($sql);

            die;
          }

          if( isset($collums[0]) && $collums[0] == 'Loja' )
          {
            continue;
          }

          if( isset($collums[0]) && $collums[0] == 'Total Geral' )
          {
            continue;
          }

          $data['loja']                       = isset($collums[0]) && $collums[0] != ''  ? trim($collums[0]) : '';
          $data['codigo_brasil']              = isset($collums[4]) && $collums[4] != ''  ? trim($collums[4]) : '';
          $data['codigo_barras']              = isset($collums[5]) && $collums[5] != ''  ? trim($collums[5]) : '';
          $data['descricao']                  = isset($collums[6]) && $collums[6] != ''  ? trim($collums[6]) : '';
          $data['linha']                      = isset($collums[7]) && $collums[7] != ''  ? trim($collums[7]) : '';
          $data['preco_venda']                = isset($collums[9]) && $collums[9] != ''  ? trim(strtr($collums[9],",",".")) : '';
          $data['valor_faturado']             = isset($collums[10]) && $collums[10] != ''  ? trim(strtr($collums[10], "," ,".")) : '';
          $data['markup']                     = isset($collums[11]) && $collums[11] != ''  ? trim(strtr($collums[11],",",".")) : '';
          $data['documento_fiscal']           = isset($collums[12]) && $collums[12] != ''  ? (int) trim($collums[12]) : '';
          $data['data_ultima_entrada']        = isset($collums[13]) && $collums[13] != ''  ? (string) date("Y-m-d", strtotime(trim(strtr($collums[13],'/','-')))) : '';
          $data['quantidade_ultima_entrada']  = isset($collums[14]) && $collums[14] != ''  ? (int) trim($collums[14]) : '';
          $data['data_saida']                 = isset($collums[15]) && $collums[15] != ''  ? (string) date("Y-m-d", strtotime(trim(strtr($collums[15],'/','-')))) : '';
          $data['quantidade_saida']           = isset($collums[16]) && $collums[16] != ''  ? (int) trim($collums[16]) : '';

          $sql = "INSERT INTO balone.lb_vendas
          SET
          loja = '".$data['loja']."',
          codigo_brasil = '".$data['codigo_brasil']."',
          codigo_barras = '".$data['codigo_barras']."',
          descricao = '".$data['descricao']."',
          linha = '".$data['linha']."',
          preco_venda = '".$data['preco_venda']."',
          valor_faturado = '".$data['valor_faturado']."',
          markup = '".$data['markup']."',
          documento_fiscal = '".$data['documento_fiscal']."',
          data_ultima_entrada = '".$data['data_ultima_entrada']."',
          quantidade_ultima_entrada = '".$data['quantidade_ultima_entrada']."',
          data_saida = '".$data['data_saida']."',
          quantidade_saida = '".$data['quantidade_saida']."' ";

          try
          {
            DB::insert($sql);
          }
          catch (Exception $e)
          {
            $this->line(''.$e->getMessage().' || '.date('d-m-Y H:i:s'));

            $sql = "UPDATE balone.lb_importacao_vendas
            SET
            observacao = '".$e->getMessage()."'
            WHERE lb_importacao_venda_id = '".$idFile."' ";

            DB::update($sql);
            die;
          }


        }

        $lines++;
      }
    }

    fclose($ponteiro);

    $sql = "UPDATE balone.lb_importacao_vendas
    SET
    import = 1
    WHERE lb_importacao_venda_id = '".$idFile."' ";

    DB::update($sql);

    unlink($file);

  }



}
