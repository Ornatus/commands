<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class balone_csb_adiciona_pedidos_automatico_lb_master extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:balone-csb-adiciona-pedidos-automatico-lb-master';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Adicionar pedidos automaticamente lovebrands master';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    $this->line('Colocando o sistema em manutenção');

    $sql = "UPDATE balone.store SET maintenance = 1 WHERE name = 'CSB'";
    DB::update($sql);

    $customers  = array();

    $sql = "SELECT c.* ,
    cp.name as profile_name,
    cp.balone_customer_profile_id as profile_id,
    cp.quantity as profile_quantity,
    (SELECT description
      FROM customer_group_description cg
      WHERE cg.customer_group_id = c.customer_group_id
      AND cg.language_id = 3
      LIMIT 1) AS type
      FROM balone_customer AS c
      LEFT JOIN balone_customer_profile cp ON cp.balone_customer_profile_id = c.customer_profile_id
      WHERE status = '1' ";

      $list_customers = DB::select($sql);

      foreach ($list_customers as $customer) {
        $customers[$customer->customer_id] = $customer;
      }

      $sql   = "SELECT distinct news_week_id FROM balone.cart LIMIT 1;";
      $query = DB::select($sql);

      $week = isset($query[0]->news_week_id) ? $query[0]->news_week_id : 0;


      $sql = "SELECT distinct product_id FROM balone.cart; ";
      $query = DB::select($sql);

      $cart_products = array();

      foreach ($query as $product) {
        $cart_products[] = $product->product_id;
      }

      $products_list = $this->getProductsBaloneByIds($cart_products);

      $sql   = "SELECT
      distinct c.customer_id
      FROM balone.cart as c
      JOIN balone_customer as bc on bc.customer_id = c.customer_id
      WHERE bc.rede = 3 ";

      $query = DB::select($sql);

      $customers_cart = $query;

      foreach ($customers_cart as $ctm)
      {

        $customer_info = $customers[$ctm->customer_id];

        $order_data                   = array();
        $order_data['invoice_no']     = 0;
        $order_data['invoice_prefix'] = 'INV-'.date('Y').'-00';
        $order_data['store_id']       = 0;
        $order_data['store_name']     = 'Balone';
        //$order_data['store_url']      = HTTPS_SERVER;
        $order_data['store_url']      = 'http://franqueado.baloneacessorios.com.br/csb/';

        $order_data['customer_id']       = $customer_info->customer_id;
        $order_data['customer_group_id'] = $customer_info->customer_group_id;
        $order_data['firstname']         = $customer_info->brazil_store_name;
        $order_data['lastname']          = $customer_info->lastname;
        $order_data['email']             = $customer_info->email;
        $order_data['telephone']         = $customer_info->telephone;

        $order_data['payment_firstname']      = "";
        $order_data['payment_lastname']       = "";
        $order_data['payment_company']        = "";
        $order_data['payment_address_1']      = "";
        $order_data['payment_address_2']      = "";
        $order_data['payment_city']           = "";
        $order_data['payment_postcode']       = "";
        $order_data['payment_zone']           = "";
        $order_data['payment_zone_id']        = "";
        $order_data['payment_country']        = "";
        $order_data['payment_country_id']     = "";
        $order_data['payment_address_format'] = "";
        $order_data['payment_custom_field']   = "";
        $order_data['payment_method']         = '';
        $order_data['comment']                = '';
        $order_data['payment_code']           = '';

        $order_data['shipping_firstname']      = '';
        $order_data['shipping_lastname']       = '';
        $order_data['shipping_company']        = '';
        $order_data['shipping_address_1']      = '';
        $order_data['shipping_address_2']      = '';
        $order_data['shipping_city']           = '';
        $order_data['shipping_postcode']       = '';
        $order_data['shipping_zone']           = '';
        $order_data['shipping_zone_id']        = '';
        $order_data['shipping_country']        = '';
        $order_data['shipping_country_id']     = '';
        $order_data['shipping_address_format'] = '';
        $order_data['shipping_custom_field']   = '';
        $order_data['shipping_code']           = '';
        $order_data['shipping_method']         = '';


        $order_data['forwarded_ip']    = '';
        $order_data['currency_code']   = 'BRL';
        $order_data['currency_id']     = '4';
        $order_data['currency_value']  = '1';
        $order_data['user_agent']      = '';
        $order_data['accept_language'] = '';
        $order_data['marketing_id']    = '';
        $order_data['commission']      = '';
        $order_data['total']           = '0';
        $order_data['affiliate_id']    = '';
        $order_data['language_id']     = '2';
        $order_data['tracking']        = '';
        $order_data['ip']              = '';
        $order_data['is_save_cart']    = '';

        $order_data['purchase_type'] = 'A';
        $order_data['news_week_id']  = $week;

        $order_id = $this->addOrder($order_data);

        $sql = "SELECT *
        FROM balone.cart
        WHERE customer_id = '" . $ctm->customer_id . "'
        AND news_week_id = '" . $week . "'";

        $query = DB::select($sql);

        $products_cart = $query;

        $total_order = 0;

        if (isset($products_cart)) {
          foreach ($products_cart as $product) {

            $product->price = $products_list[$product->product_id]['franchise_price'];
            $product->name  = $products_list[$product->product_id]['model'];
            $product->model = $products_list[$product->product_id]['model'];
            $product->total = ($product->profile_quantity * round($product->price, 2));

            $sql = "INSERT INTO balone.order_product
            SET
            profile_quantity = '" . $product->profile_quantity . "',
            extra_quantity = '0',
            reserve_quantity = '0',
            purchase_type='A',
            order_id = '" . (int) $order_id . "',
            product_id = '" . (int) $product->product_id . "',
            name = '" . $product->name . "',
            model = '" . $product->model . "',
            quantity = '" . (int) $product->profile_quantity . "',
            price = '" . (float) $product->price . "',
            total = '" . (float) round($product->total, 2) . "',
            tax = '0',
            reward = '0'";

            DB::insert($sql);

            $product->order_id = $order_id;

            $total_order = $total_order + (float) $product->total;

          }
        }

        $sql = "UPDATE balone.order
        SET total = '" . round($total_order, 2) . "'
        WHERE order_id = '" . $order_id . "';";

        DB::update($sql);

        $sql = "DELETE FROM balone.cart
        WHERE customer_id  = '" . $ctm->customer_id . "'; ";

        DB::select($sql);

      }

      $sql = "TRUNCATE balone.cart ";
      DB::select($sql);

      $sql = "UPDATE balone.store SET maintenance = 0 WHERE name = 'CSB'";
      DB::update($sql);

    }

    public function getProductsBaloneByIds($product_ids){
      $return = array();

      foreach($product_ids as $id)
      {
        $product_info = $this->getProductBaloneById($id);

        $return[$id]=$product_info;
      }

      return $return;

    }

    public function getProductBaloneById($product_id) {

      $sql = " SELECT DISTINCT *,
      p.model AS name,
      p.image,
      m.name AS manufacturer,
      p.ppm_product,
      p.brazil_code,

      (SELECT name FROM balone_product_family
        WHERE product_family_id = p.product_family_id LIMIT 1) as family_name,

        (SELECT is_publicity FROM balone_product_family
          WHERE product_family_id = p.product_family_id LIMIT 1) as is_publicity,

          (SELECT cd.name
            FROM product_to_category ptc
            JOIN category_description cd on cd.category_id = ptc.category_id
            WHERE ptc.product_id = p.product_id
            AND cd.language_id = 3 LIMIT 1) AS category_description,

            (SELECT nwpp.quantity FROM balone_news_week_product_profile nwpp
              JOIN balone_news_week_product nwp ON nwp.news_week_product_id = nwpp.news_week_product_id
              JOIN balone_news_week nw ON nw.news_week_id = nwp.news_week_id
              WHERE nw.active = 1
              AND current_date >= nw.date_start
              AND current_date <= nw.date_end
              AND nwp.product_id = 'p.product_id'
              AND nwp.active = 1 LIMIT 1) as profile_quantity,

              (SELECT product_id FROM po_products pp
                WHERE pp.product_id = p.product_id limit 1
              ) AS reorder,

              (SELECT cd.category_id
                FROM product_to_category ptc
                JOIN category_description cd on cd.category_id = ptc.category_id
                WHERE ptc.product_id = p.product_id
                AND cd.language_id = 3 LIMIT 1) AS category_id,

                (SELECT suggested_price FROM product_fixed_price_brazil
                  WHERE product_id = p.product_id) as retail_price,

                  CASE WHEN p.ppm_product = 1 THEN
                  ( SELECT fixed_price+( (suggested_price * 5 )/100) FROM product_fixed_price_brazil
                  WHERE product_id = p.product_id)
                  ELSE
                  ( SELECT fixed_price+( (suggested_price * 5 )/100) FROM product_fixed_price_brazil
                  WHERE product_id = p.product_id)
                  END
                  AS franchise_price,

                  ( SELECT fixed_price FROM product_fixed_price_brazil
                    WHERE product_id = p.product_id ) AS employe_price,

                    (SELECT price
                      FROM product_discount pd2
                      WHERE pd2.product_id = p.product_id
                      AND pd2.quantity = '1'
                      AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW())
                      AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW()))
                      ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount,

                      (SELECT price FROM product_special ps
                        WHERE ps.product_id = p.product_id
                        AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW())
                        AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))
                        ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,

                        (SELECT description FROM product_description pd WHERE language_id = 3
                          AND pd.product_id = p.product_id limit 1) AS product_description,


                          (SELECT points FROM product_reward pr
                            WHERE pr.product_id = p.product_id
                            LIMIT 1) AS reward,

                            '' AS stock_status,

                            '' AS weight_class,

                            '' AS length_class,

                            '' AS rating,

                            '' AS reviews,
                            p.sort_order ,

                            p.ppe_quantity_over AS quantity

                            FROM  product p
                            LEFT JOIN  product_description pd ON (p.product_id = pd.product_id)
                            LEFT JOIN  product_to_store p2s ON (p.product_id = p2s.product_id)
                            LEFT JOIN  manufacturer m ON (p.manufacturer_id = m.manufacturer_id)
                            WHERE p.product_id = '" . (int)$product_id . "'
                            AND pd.language_id = 3
                            AND p2s.store_id = 0" ;

                            $query = DB::select($sql);

                            if (count($query) > 0)
                            {

                              return array(
                                'product_id'       => $query[0]->product_id,
                                'name'             => $query[0]->name,
                                'description'      => $query[0]->description,
                                'brazil_code'      => $query[0]->brazil_code,
                                'product_description' => $query[0]->product_description,
                                'meta_description' => $query[0]->meta_description,
                                'meta_keyword'     => $query[0]->meta_keyword,
                                'tag'              => $query[0]->tag,
                                'model'            => $query[0]->model,
                                'sku'              => $query[0]->sku,
                                'upc'              => $query[0]->upc,
                                'ean'              => $query[0]->ean,
                                'jan'              => $query[0]->jan,
                                'isbn'             => $query[0]->isbn,
                                'mpn'              => $query[0]->mpn,
                                'location'         => $query[0]->location,
                                'quantity'         => $query[0]->quantity,
                                'stock_status'     => $query[0]->stock_status,
                                'image'            => $query[0]->image,
                                'manufacturer_id'  => $query[0]->manufacturer_id,
                                'manufacturer'     => $query[0]->manufacturer,
                                'price'            => ($query[0]->discount ? $query[0]->discount : $query[0]->price),
                                'special'          => $query[0]->special,
                                'reward'           => $query[0]->reward,
                                'points'           => $query[0]->points,
                                'tax_class_id'     => $query[0]->tax_class_id,
                                'date_available'   => $query[0]->date_available,
                                'weight'           => $query[0]->weight,
                                'weight_class_id'  => $query[0]->weight_class_id,
                                'length'           => $query[0]->length,
                                'width'            => $query[0]->width,
                                'height'           => $query[0]->height,
                                'length_class_id'  => $query[0]->length_class_id,
                                'subtract'         => $query[0]->subtract,
                                'rating'           => round($query[0]->rating),
                                'reviews'          => $query[0]->reviews ? $query[0]->reviews : 0,
                                'minimum'          => $query[0]->minimum,
                                'sort_order'       => $query[0]->sort_order,
                                'status'           => $query[0]->status,
                                'date_added'       => $query[0]->date_added,
                                'date_modified'    => $query[0]->date_modified,
                                'viewed'           => $query[0]->viewed,
                                'suggested_price'  => $query[0]->retail_price,
                                'franchise_price'  => $query[0]->franchise_price,
                                'employe_price'  => $query[0]->employe_price,
                                'ppm_product'      => $query[0]->ppm_product,
                                'category_description'=> $query[0]->category_description,
                                'color3'=> $query[0]->color3,
                                'category_id'=> $query[0]->category_id,
                                'reorder'=> $query[0]->reorder,
                                'family_name'=> $query[0]->family_name,
                                'is_publicity'=> $query[0]->is_publicity

                              );
                            } else {
                              return false;
                            }
                          }


                          public function addOrder($data)
                          {
                            $is_save_cart = '';

                            if(!isset($data['is_save_cart']))
                            {
                              $is_save_cart = $data['is_save_cart'];
                            }


                            $data['custom_field'] = isset($data['custom_field']) ? json_encode($data['custom_field']) : "";
                            $data['payment_custom_field'] = isset($data['payment_custom_field']) && $data['payment_custom_field'] != '' ? json_encode($data['payment_custom_field']) : "";
                            $data['shipping_custom_field'] = isset($data['shipping_custom_field']) && $data['shipping_custom_field'] != '' ? json_encode($data['shipping_custom_field']) : "";

                            $sql = "INSERT INTO balone.order
                            SET invoice_prefix = '" . $data['invoice_prefix'] . "',
                            store_id = '" . (int) $data['store_id'] . "',
                            store_name = '" . $data['store_name'] . "',
                            store_url = '" . $data['store_url'] . "',
                            customer_id = '" . (int) $data['customer_id'] . "',
                            customer_group_id = '" . (int) $data['customer_group_id'] . "',
                            firstname = '" . $data['firstname'] . "',
                            lastname = '" . $data['lastname'] . "',
                            email = '" . $data['email'] . "',
                            telephone = '" . $data['telephone'] . "',
                            custom_field = '" . $data['custom_field'] . "',
                            payment_firstname = '" . $data['payment_firstname'] . "',
                            payment_lastname = '" . $data['payment_lastname'] . "',
                            payment_company = '" . $data['payment_company'] . "',
                            payment_address_1 = '" . $data['payment_address_1'] . "',
                            payment_address_2 = '" . $data['payment_address_2'] . "',
                            payment_city = '" . $data['payment_city'] . "',
                            payment_postcode = '" . $data['payment_postcode'] . "',
                            payment_country = '" . $data['payment_country'] . "',
                            payment_country_id = '" . (int) $data['payment_country_id'] . "',
                            payment_zone = '" . $data['payment_zone'] . "',
                            payment_zone_id = '" . (int) $data['payment_zone_id'] . "',
                            news_week_id = '" . (int) $data['news_week_id'] . "',
                            payment_address_format = '" . $data['payment_address_format'] . "',
                            payment_custom_field = '" . $data['payment_custom_field'] . "',
                            payment_method = '" . $data['payment_method'] . "',
                            payment_code = '" . $data['payment_code'] . "',
                            shipping_firstname = '" . $data['shipping_firstname'] . "',
                            shipping_lastname = '" . $data['shipping_lastname'] . "',
                            shipping_company = '" . $data['shipping_company'] . "',
                            shipping_address_1 = '" . $data['shipping_address_1'] . "',
                            shipping_address_2 = '" . $data['shipping_address_2'] . "',
                            shipping_city = '" . $data['shipping_city'] . "',
                            shipping_postcode = '" . $data['shipping_postcode'] . "',
                            shipping_country = '" . $data['shipping_country'] . "',
                            shipping_country_id = '" . (int) $data['shipping_country_id'] . "',
                            shipping_zone = '" . $data['shipping_zone'] . "',
                            purchase_type = '" . $data['purchase_type'] . "',
                            shipping_zone_id = '" . (int) $data['shipping_zone_id'] . "',
                            shipping_address_format = '" . $data['shipping_address_format'] . "',
                            shipping_custom_field = '" . $data['shipping_custom_field'] . "',
                            shipping_method = '" . $data['shipping_method'] . "',
                            shipping_code = '" . $data['shipping_code'] . "',
                            comment = '" . $data['comment'] . "',
                            total = '" . (float) $data['total'] . "',
                            affiliate_id = '" . (int) $data['affiliate_id'] . "',
                            commission = '" . (float) $data['commission'] . "',
                            marketing_id = '" . (int) $data['marketing_id'] . "',
                            tracking = '" . $data['tracking'] . "',
                            language_id = '" . (int) $data['language_id'] . "',
                            currency_id = '" . (int) $data['currency_id'] . "',
                            currency_code = '" . $data['currency_code'] . "',
                            currency_value = '" . (float) $data['currency_value'] . "',
                            ip = '" . $data['ip'] . "',
                            forwarded_ip = '" . $data['forwarded_ip'] . "',
                            user_agent = '" . $data['user_agent'] . "',
                            accept_language = '" . $data['accept_language'] . "',
                            date_added = NOW(),
                            date_modified = NOW(),
                            order_status_id = 1,
                            is_save_cart = '".$is_save_cart."'  " ;

                            DB::insert($sql);
                            $id = DB::getPdo()->lastInsertId();
                            return $id;

                          }





                        }
