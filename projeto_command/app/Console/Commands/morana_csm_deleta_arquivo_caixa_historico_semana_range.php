<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class morana_csm_deleta_arquivo_caixa_historico_semana_range extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-csm-deleta-arquivo-caixa-historico-semana-range';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Deleta arquivos CSV com mais de 30 dias';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    $this->line('Coletando arquivos - '.date('d-m-Y H:i:s'));

    $sql = "SELECT
    *,
    DATEDIFF(current_date(),date(date_solicited)) as diff_days
    from morana2.report_itens_caixa_csm
    WHERE status = 2
    AND DATEDIFF(current_date(),date(date_solicited)) > 30
    AND active = 1";

    $files =  DB::select($sql);

    foreach ($files as $file)
    {
      $this->line('Removendo - '.$file->file_name. ' - '.date('d-m-Y H:i:s'));
      unlink(env('PATH_REPORT_FILE')."/".$file->file_name);

      $sql = "UPDATE morana2.report_itens_caixa_csm
      SET
      active = 0
      WHERE report_itens_caixa_csm_id = '".$file->report_itens_caixa_csm_id."'
      AND active = 1
      AND status = 2";

      DB::update($sql);

    }

    $this->line('Limpeza finalizada '.date('d-m-Y H:i:s'));

  }
}
