<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class morana_csm_adiciona_produtos_carrinho extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:morana-csm-adiciona-produtos-carrinho';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $this->line('Colocando o sistema em manutenção');

      $sql = "UPDATE webstore.store SET maintenance = 1 WHERE name = 'PPE'";
      DB::update($sql);

      $sql = "TRUNCATE webstore.cart;";

      DB::select($sql);

      $sql = "SELECT * FROM customer c
      JOIN customer_profile cp
      ON cp.customer_profile_id = c.customer_profile_id
      WHERE c.ppe_access = 1 ";

      $customers =  DB::select($sql);

      foreach ($customers as $customer)
      {
        $this->line($customer->customer_id);
        //Pega os produtos da semana atual*************************
        $sql = "SELECT
        nwp.*,
        (SELECT quantity FROM news_week_product_profile
          WHERE active = 1
          AND news_week_product_id = nwp.news_week_product_id
          AND profile_slug='".$customer->slug."'
          LIMIT 1) as profile_quantity
          FROM news_week_product nwp
          JOIN news_week nw ON nw.news_week_id = nwp.news_week_id
          JOIN news_week_family nwf ON nwf.news_week_id = nw.news_week_id AND nwp.product_family_id = nwf.product_family_id AND nwf.active = 1
          JOIN product p ON nwp.product_id = p.product_id
          WHERE
          nwp.active = 1
          AND
          current_date >= nw.date_start
          AND
          current_date <= nw.date_end
          AND nw.confirmed = 'S'
          AND nw.active = 1
          ORDER BY nwp.address_index;";

          $products = DB::select($sql);


          if(count($products)==0)
          {
            return false;
          }
          //***********************************************************

          $cart = array();


          foreach($products as $product)
          {

            $cart['customer_id'] = $customer->customer_id;
            $cart['product_id']  = $product->product_id;
            $cart['quantity']    = $product->profile_quantity;
            $cart['profile_quantity']    = $product->profile_quantity;

            //Se for a loja ecommerce, pega a quantidade da coluna quantity_ecommerce
            if($customer['brazil_store_id'] == 'ECO')
            {
              $cart['quantity']    = $product['quantity_ecommerce'];
              $cart['profile_quantity']    = $product['quantity_ecommerce'];
            }
            
            $cart['news_week_id']    = $product->news_week_id;
            $cart['purchase_type']    = 'N';
            $cart['type'] = $product->type;

            $sql ="INSERT INTO webstore.cart
            SET
            customer_id = '".$cart['customer_id']."',
            product_id = '".$cart['product_id']."',
            quantity = '".$cart['quantity']."',
            profile_quantity = '".$cart['profile_quantity']."',
            purchase_type = '".$cart['purchase_type']."',
            news_week_id = '".$cart['news_week_id']."',
            type = '".$cart['type']."',
            date_added = current_timestamp,
            `option` = '[]'; " ;

            DB::insert($sql);

          }

        }

        $sql = "UPDATE webstore.store SET maintenance = 0 WHERE name = 'PPE'";
        DB::update($sql);

      }
}
