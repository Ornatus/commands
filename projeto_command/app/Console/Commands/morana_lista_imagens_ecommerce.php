<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class morana_lista_imagens_ecommerce extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-lista-imagens-ecommerce';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Command description';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    $this->line('Iniciando... '.date('d-m-Y H:i:s'));

    $this->line('Coletando produtos LV '.date('d-m-Y H:i:s'));

    $sql = "SELECT
    SUBSTRING(b.modelo, 18, 2) as cor,
    b.*,
    CASE
    WHEN SUBSTRING(b.modelo, 8, 3) NOT IN ('ANK' , 'ERR', 'RNG', 'BRC', 'SET', 'ANK', 'HAR', 'NCL', 'MAT', 'PDT') THEN 'OUTROS'
    ELSE SUBSTRING(b.modelo, 8, 3)
    END AS categoria,
    pd.brazil_description,
    pf.suggested_price,
    ps.segment_id,
    ps.subcategory_id,
    ps.sub_category2_id,
    ps.name,
    TRIM(p.brazil_code) as brazil_code,
    p.image
    FROM
    (
    SELECT
        'LOJAVIRTUAL' AS plataforma,
        o.date_added AS data,
        o.lv_order_id AS pedido,
        c.customer_id AS customer_id,
        c.brazil_store_id AS area,
        c.brazil_store_name AS loja,
        p.product_id AS product_id,
        p.brazil_code AS brazil_code,
        p.model AS modelo,
        SUBSTR(p.model, 8, 3) AS categoria,
        fp.suggested_price AS preco,
        '' AS semana,
        CAST(o.date_box AS DATE) AS date_box,
        op.quantity_partial AS quantidade
    FROM
        ((((lv_order_product op
        JOIN lv_order o ON ((o.lv_order_id = op.lv_order_id)))
        JOIN customer c ON (((c.customer_id = o.customer_id)
            AND (o.type_user_id = 1))))
        JOIN product p ON ((p.product_id = op.product_id)))
        JOIN product_fixed_price_brazil fp ON ((fp.product_id = p.product_id)))
    WHERE
        (o.status_id IN (3,8)) AND c.customer_id = 489 AND c.brazil_store_id = 'ECO'  ) as b
    JOIN
    customer AS c ON c.customer_id = b.customer_id
    JOIN
    product AS p ON p.product_id = b.product_id
    LEFT JOIN
    product_description AS pd ON pd.product_id = p.product_id AND pd.language_id = 3
    LEFT JOIN
    product_fixed_price_brazil AS pf ON pf.product_id = b.product_id
    LEFT JOIN
    product_sub_category as ps ON ps.product_id = b.product_id AND ps.active = 1
    WHERE
    c.brazil_store_id = 'ECO'  ";

    $products_lv =   DB::select($sql);

    $this->line('Coletando produtos PPM '.date('d-m-Y H:i:s'));

    $sql = "SELECT
    SUBSTRING(b.modelo, 18, 2) as cor,
    b.*,
    CASE
    WHEN SUBSTRING(b.modelo, 8, 3) NOT IN ('ANK' , 'ERR', 'RNG', 'BRC', 'SET', 'ANK', 'HAR', 'NCL', 'MAT', 'PDT') THEN 'OUTROS'
    ELSE SUBSTRING(b.modelo, 8, 3)
    END AS categoria,
    pd.brazil_description,
    pf.suggested_price,
    ps.segment_id,
    ps.subcategory_id,
    ps.sub_category2_id,
    ps.name,
    TRIM(p.brazil_code) as brazil_code,
    p.image
    FROM
    ( SELECT
        'PPM' AS plataforma,
        o.date_added AS data,
        o.order_id AS pedido,
        c.customer_id AS customer_id,
        c.brazil_store_id AS area,
        c.brazil_store_name AS loja,
        p.product_id AS product_id,
        p.brazil_code AS brazil_code,
        p.model AS modelo,
        SUBSTR(p.model, 8, 3) AS categoria,
        fp.suggested_price AS preco,
        op.week_number AS semana,
        op.date_picked AS date_box,
        op.quantity AS quantidade
    FROM
        ((((order_product op
        JOIN `order` o ON ((o.order_id = op.order_id)))
        JOIN customer c ON ((c.customer_id = o.customer_id)))
        JOIN product p ON ((p.product_id = op.product_id)))
        JOIN product_fixed_price_brazil fp ON ((fp.product_id = p.product_id)))
    WHERE
        ((op.order_product_status_id IN (13,8)
            AND (op.product_id > 0))) AND c.customer_id = 489 AND c.brazil_store_id = 'ECO' ) as b
    LEFT JOIN(
      SELECT
      lv.customer_id,
      lvo.week_number,
      max(lvo.lv_confirm_picking_id) as  lv_confirm_picking_id
      FROM
      lv_confirm_picking_orders lvo
      join lv_confirm_picking lv on lv.lv_confirm_picking_id = lvo.lv_confirm_picking_id
      where lvo.type_system = 'ppm'
      and lv.active = 1
      and lv.is_admin = 0
      and lvo.active = 1
      and lv.tipo_usuario = 1
      group by 1,2) l on l.week_number = b.semana AND b.customer_id = l.customer_id
      JOIN
      customer AS c ON c.customer_id = b.customer_id
      JOIN
      product AS p ON p.product_id = b.product_id
      LEFT JOIN
      product_description AS pd ON pd.product_id = p.product_id AND pd.language_id = 3
      LEFT JOIN
      product_fixed_price_brazil AS pf ON pf.product_id = b.product_id
      LEFT JOIN
      product_sub_category as ps ON ps.product_id = b.product_id AND ps.active = 1
      WHERE
      c.brazil_store_id = 'ECO'  ";

      $products_ppm =   DB::select($sql);

      $this->line('Coletando produtos CSM '.date('d-m-Y H:i:s'));

      $sql = "SELECT
      SUBSTRING(b.modelo, 18, 2) as cor,
      b.*,
      CASE
      WHEN SUBSTRING(b.modelo, 8, 3) NOT IN ('ANK' , 'ERR', 'RNG', 'BRC', 'SET', 'ANK', 'HAR', 'NCL', 'MAT', 'PDT') THEN 'OUTROS'
      ELSE SUBSTRING(b.modelo, 8, 3)
      END AS categoria,
      pd.brazil_description,
      pf.suggested_price,
      ps.segment_id,
      ps.subcategory_id,
      ps.sub_category2_id,
      (SELECT max(lvo.lv_confirm_picking_id) lv_confirm_picking_id
      FROM
      lv_confirm_picking_orders lvo
      join lv_confirm_picking lv on lv.lv_confirm_picking_id = lvo.lv_confirm_picking_id
      where lvo.type_system = 'csm'
      and lv.active = 1
      and lv.is_admin = 0
      and lvo.active = 1
      and lvo.order_id = b.pedido limit 1) as liberacao,
      ps.name,
      TRIM(p.brazil_code) as brazil_code,
      p.image
      FROM (
    SELECT
        'CSM' AS plataforma,
        o.date_added AS data,
        o.order_id AS pedido,
        c.customer_id AS customer_id,
        c.brazil_store_id AS area,
        c.brazil_store_name AS loja,
        p.product_id AS product_id,
        p.brazil_code AS brazil_code,
        p.model AS modelo,
        SUBSTR(p.model, 8, 3) AS categoria,
        fp.suggested_price AS preco,
        o.news_week_id AS semana,
        dt_box.date_box AS date_box,
        (CASE
            WHEN (o.purchase_type <> 'R') THEN (COALESCE(op.quantity, 0) + COALESCE(op.extra_quantity, 0))
            ELSE COALESCE(op.quantity_partial, 0)
        END) AS quantidade
    FROM
        (((((webstore.order_product op
        JOIN webstore.`order` o ON ((o.order_id = op.order_id)))
        JOIN morana2.customer c ON ((c.customer_id = o.customer_id)))
        JOIN morana2.product p ON ((p.product_id = op.product_id)))
        JOIN morana2.product_fixed_price_brazil fp ON ((fp.product_id = p.product_id)))
        LEFT JOIN (SELECT
            morana2.order_status_log.order_id AS order_id,
                CAST(MAX(morana2.order_status_log.date_added)
                    AS DATE) AS date_box
        FROM
            morana2.order_status_log
        WHERE
            (morana2.order_status_log.order_status_id = 3)
        GROUP BY morana2.order_status_log.order_id) dt_box ON ((dt_box.order_id = o.order_id)))
    WHERE
        ((o.order_status_id IN (3,8))
            AND ((op.quantity > 0)
            OR (op.extra_quantity > 0))) AND c.customer_id = 489 AND c.brazil_store_id = 'ECO') as b
      JOIN
      customer AS c ON c.customer_id = b.customer_id
      JOIN
      product AS p ON p.product_id = b.product_id
      LEFT JOIN
      product_description AS pd ON pd.product_id = p.product_id AND pd.language_id = 3
      LEFT JOIN
      product_fixed_price_brazil AS pf ON pf.product_id = b.product_id
      LEFT JOIN
      product_sub_category as ps ON ps.product_id = b.product_id AND ps.active = 1
      WHERE
      c.brazil_store_id = 'ECO'  ";

      $products_csm =   DB::select($sql);

      $http = "https://franqueado.morana.com.br/ppm/index.php?route=product/image&width=%s&height=%s&image=%s";

      $products = array_merge($products_lv,$products_ppm,$products_csm);

      $this->line('Criando Excel... '.date('d-m-Y H:i:s'));

      ini_set("memory_limit","2048M");

      $file = env('PATH_REPORT_FILE')."/lista_de_imagens_ecommerce.csv";
      $myfile = fopen($file, "w") or die("Unable to open file!");

      $https = env('HTTP_CREATE_IMAGE_ECOMMERCE');

      $html = "CODIGO;";
      $html .= "IMAGEM;";
      $html .= "\n";

      $this->line('Coletando produtos LV '.date('d-m-Y H:i:s'));

      $sql = "SELECT
      *
      FROM log_import_image_ecommerce ";

      $this->line('Coletando informacoes... '.date('d-m-Y H:i:s'));

      $results = DB::connection('mysql2')->select($sql);

      foreach ($products as $product) {
        foreach ($results as $result) {

          if($product->product_id == $result->product_id ){
            $name_folder =  substr($product->brazil_code, 0, 4);
            $brazil_code = preg_replace("@(_\d+.jpg)@is","",$result->image_name);
            $html .= $brazil_code.";";
            $html .= env('HTTP_CREATE_IMAGE_ECOMMERCE').$name_folder."/".$result->image_name.";";
            $html .= "\n";
          }

        }
      }

      fwrite($myfile, $html);

      $this->line('Fim '.date('d-m-Y H:i:s'));


      fclose($myfile);

    }
  }
