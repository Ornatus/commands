<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class suporte_deleta_backup extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:suporte-deleta-backup';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Deleta arquivos de backups antigos das lojas no servidor de cuponagem da VIP';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    $this->line('Listando Lojas - '.date('d-m-Y H:i:s'));

    $sql = "SELECT
    brazil_store_id
    FROM customer
    WHERE ppe_access = 1
    ORDER BY brazil_store_id ASC;";

    $customers =  DB::select($sql);

    //pego todos os arquivos de bkp da pasta
    $files = scandir(env('BACKUP_FILES'));

    foreach ($customers as $customer)
    {
      $this->line('Verificando Arquivos da Loja:'.$customer->brazil_store_id.' - '.date('d-m-Y H:i:s'));
      $files_customer = array();

      //inicio a verificação arquivo por arquivo
      foreach($files as $file)
      {
        //verifico se o arquivo é da loja atual do foreach
        if(preg_match('@(Area'.$customer->brazil_store_id.')@is',$file))
        {
          $files_customer[] = $file;
        }
      }
      print_R($files_customer);
      //se tiver +2 arquivos começo a deletar os antigos
      if(count($files_customer) > 2)
      {
        array_pop($files_customer);
        array_pop($files_customer);

        foreach ($files_customer as $file)
        {
          $this->line('Removendo: '.$file);
          //unlink(env('BACKUP_FILES').$file);
        }
      }


    }


  }

}
