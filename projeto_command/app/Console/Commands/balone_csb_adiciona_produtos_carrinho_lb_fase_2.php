<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class balone_csb_adiciona_produtos_carrinho_lb_fase_2 extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:balone-csb-adiciona-produtos-carrinho-lb-fase-2';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Command description';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {

    $this->line('Criando carrinho das lovebrands fase 2');

    $sql = "UPDATE balone.store
    SET maintenance = 1
    WHERE name = 'CSB'";
    DB::update($sql);

    $sql = "SELECT distinct(news_week_id)
    FROM morana2.balone_news_week WHERE active = 1
    AND date_start <= CURRENT_DATE
    AND date_end >= CURRENT_DATE LIMIT 1;";

    $week = DB::select($sql);

    if(count($week)== 0){
      $this->line('Nenhuma semana encontrada');
      return false;
    }

    $week = $week[0]->news_week_id;

    $sql = "SELECT *
    FROM morana2.balone_customer
    WHERE rede = 2 AND ppe_access = 1
    AND customer_id not in (SELECT customer_id from balone.order where news_week_id = '".$week."') ";

    $customers = DB::select($sql);

    $sql = "SELECT *
    FROM morana2.balone_customer
    WHERE rede = 2 AND ppe_access = 1 ";

    $master = DB::select($sql);
    $qtd_lb = count($master);

    $sql = "SELECT bnwp.product_id,
    bnwp.news_week_product_id,
    quantity,
    coalesce(bnwp.quantity_lb_reserve,0) as quantity_lb_reserve,
    (SELECT
      COALESCE(SUM(quantity),0) + COALESCE(SUM(extra_quantity),0) AS quantity
      FROM balone.order_product op
      JOIN balone.order o ON o.order_id = op.order_id
      WHERE o.news_week_id = bnwp.news_week_id
      AND op.product_id = bnwp.product_id
      AND o.purchase_type IN('N','A') LIMIT 1) AS orders
      FROM morana2.balone_news_week_product bnwp
      JOIN morana2.balone_news_week bnw ON bnw.news_week_id = bnwp.news_week_id
      WHERE bnwp.active = 1
      AND bnw.active =1
      AND bnw.news_week_id = '".$week."'
      ORDER BY address_index;";

      $product_week = DB::select($sql);

    /*foreach ($product_week as $prod_week)
      {

        $quantity = ($prod_week->quantity - $prod_week->quantity_lb_reserve) - $prod_week->orders;

        $quantity = $quantity + $prod_week->quantity_lb_reserve;

        $data['quantity_lb'] = ($quantity > 0) ? $quantity : 0;

        if( $data['quantity_lb'] >= (int)$qtd_lb )
        {
          $quantity_over = (int)$data['quantity_lb'] - (int)$qtd_lb;
        }
        else
        {
          $quantity_over = (int)$data['quantity_lb'];
        }

        $data['quantity_over_lb'] = ($quantity_over > 0) ? $quantity_over : 0;

        $sql = "UPDATE morana2.balone_news_week_product
        SET quantity_lb = '".$data['quantity_lb']."',
        quantity_over_lb = '".$data['quantity_over_lb']."',
        quantity_orders = '".$prod_week->orders."'
        WHERE news_week_product_id = '".$prod_week->news_week_product_id."' ";

        DB::update($sql);

      }*/

      $sql = "SELECT bnwp.product_id,
      bnwp.news_week_product_id,
      bnwp.quantity_over_lb,
      bnwp.type,
      bnw.news_week_id,
      quantity_lb
      FROM morana2.balone_news_week_product bnwp
      JOIN morana2.balone_news_week bnw ON bnw.news_week_id = bnwp.news_week_id
      WHERE bnwp.active = 1
      AND bnw.active =1
      AND bnw.news_week_id = '".$week."'
      AND bnwp.quantity_lb > 0
      ORDER BY address_index;";

      $product_cart = DB::select($sql);

      foreach ($customers as $customer)
      {
        foreach ($product_cart as $product)
        {
          $cart['quantity'] = '0';
          $cart['product_id'] = $product->product_id;
          $cart['customer_id'] = $customer->customer_id;
          $cart['purchase_type'] = 'N';
          $cart['profile_quantity'] = '0';
          $cart['news_week_id'] = $product->news_week_id;
          $cart['type'] = $product->type;

          /*if($product->quantity_lb >= $qtd_lb && $customer->rede == 2 )
          {
            $cart['quantity'] = '1';
            $cart['profile_quantity'] = '1';

          }*/

          $sql = "INSERT INTO balone.cart
          SET customer_id = '" . $cart['customer_id'] . "',
          profile_quantity = '" . $cart['profile_quantity'] . "',
          extra_quantity = '0',
          reserve_quantity = '0',
          purchase_type= 'N',
          product_id = '" . (int) $cart['product_id'] . "',
          quantity = '" . (int) $cart['quantity'] . "',
          news_week_id = '".(int) $cart['news_week_id']."',
          type = '".(int) $cart['type']."'; ";

          DB::insert($sql);

        /*  $sql ="UPDATE morana2.product
          SET`status`=1
          WHERE product_id = '".$cart['product_id']."'
          AND `status`=0 ; ";

          DB::update($sql);*/

        }
      }


    /*  $sql = "SELECT
      nwp.news_week_product_id,
      nwp.quantity,
      (SELECT
        COALESCE(SUM(quantity),0) + COALESCE(SUM(extra_quantity),0) AS quantity
        FROM balone.cart c
        WHERE c.news_week_id = nwp.news_week_id
        AND c.product_id = nwp.product_id
        LIMIT 1) AS cart,
        nwp.quantity - (SELECT
          COALESCE(SUM(quantity),0) + COALESCE(SUM(extra_quantity),0) AS quantity
          FROM balone.cart c
          WHERE c.news_week_id = nwp.news_week_id
          AND c.product_id = nwp.product_id
          LIMIT 1) as sobra
          FROM balone_news_week_product nwp
          JOIN balone_news_week nw ON nw.news_week_id = nwp.news_week_id
          JOIN balone_news_week_family nwf ON nwf.news_week_id = nw.news_week_id AND nwp.product_family_id = nwf.product_family_id AND nwf.active = 1
          JOIN product p ON nwp.product_id = p.product_id
          WHERE
          nwp.active = 1
          AND
          current_date >= nw.date_start
          AND
          current_date <= nw.date_end
          AND nw.confirmed = 'S'
          AND nw.active = 1
          ORDER BY nwp.address_index;";

          $products = DB::select($sql);

          foreach ($products as $product) {
            $sql = "UPDATE balone_news_week_product
            SET
            quantity_over = '".$product->sobra."'
            WHERE news_week_product_id = '".$product->news_week_product_id."' ";

            DB::update($sql);
          }*/

          $this->line('Fim da criancao dos carrinho das lovebrands');

          $sql = "UPDATE balone.store
          SET maintenance = 0
          WHERE name = 'CSB'";

          DB::update($sql);


        }
      }
