<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class balone_itens_caixa extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:balone-itens-caixa';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Command description';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {

    ini_set("memory_limit","2048M");

    $file = env('PATH_REPORT_FILE')."/produtos_caixa_balone.csv";
    $myfile = fopen($file, "w") or die("Unable to open file!");


    $html = "Plataforma;";
    $html .= "Data;";
    $html .= "Pedido;";
    $html .= "Area;";
    $html .= "Loja;";
    $html .= "Modelo;";
    $html .= "Codigo;";
    $html .= "Categoria;";
    $html .= "Preco;";
    $html .= "Semana;";
    $html .= "DataCaixa;";
    $html .= "Quantidade;";
    $html .= "Liberacao;";
    $html .= "\n";

    fwrite($myfile, $html);

    $this->line('Escreveu cabecalho '.date('d-m-Y H:i:s'));

    $sql = "SELECT
    b.*,
    (SELECT max(lvo.lv_confirm_picking_id) lv_confirm_picking_id
    FROM
    lv_confirm_picking_orders lvo
    join lv_confirm_picking lv on lv.lv_confirm_picking_id = lvo.lv_confirm_picking_id
    where lvo.type_system = 'csb'
    and lv.active = 1
    and lv.is_admin = 0
    and lvo.active = 1
    and lvo.order_id = b.pedido limit 1) as liberacao
    FROM (SELECT
      'CSB' AS plataforma,
      o.date_added AS data,
      o.order_id AS pedido,
      c.customer_id AS customer_id,
      c.brazil_store_id AS area,
      c.brazil_store_name AS loja,
      p.product_id AS product_id,
      p.brazil_code AS brazil_code,
      p.model AS modelo,
      SUBSTR(p.model, 8, 3) AS categoria,
      fp.suggested_price AS preco,
      o.news_week_id AS semana,
      dt_box.date_box AS date_box,
      (CASE
        WHEN (o.purchase_type <> 'R') THEN (COALESCE(op.quantity, 0) + COALESCE(op.extra_quantity, 0))
        ELSE COALESCE(op.quantity_partial, 0)
        END) AS quantidade
        FROM
        (((((balone.order_product op
          JOIN balone.order o ON ((o.order_id = op.order_id)))
          JOIN morana2.balone_customer c ON ((c.customer_id = o.customer_id)))
          JOIN morana2.product p ON ((p.product_id = op.product_id)))
          JOIN morana2.product_fixed_price_brazil fp ON ((fp.product_id = p.product_id)))
          LEFT JOIN (SELECT
            morana2.balone_order_status_log.order_id AS order_id,
            CAST(MAX(morana2.balone_order_status_log.date_added)
            AS DATE) AS date_box
            FROM
            morana2.balone_order_status_log
            WHERE
            (morana2.balone_order_status_log.order_status_id = 3)
            GROUP BY morana2.balone_order_status_log.order_id) dt_box ON ((dt_box.order_id = o.order_id)))
            WHERE
            ((o.order_status_id = 3)
            AND ((op.quantity > 0)
            OR (op.extra_quantity > 0)))) b";

            $results_csb =  DB::select($sql);

            $this->line('Terminou query do CSB '.date('d-m-Y H:i:s'));

            $html = "";

            foreach ($results_csb as $result) {

              $html .= $result->plataforma.";";
              $html .= date("d/m/Y",strtotime($result->data)).";";
              $html .= $result->pedido.";";
              $html .= $result->area.";";
              $html .= $result->loja.";";
              $html .= $result->modelo.";";
              $html .= $result->brazil_code.";";
              $html .= $result->categoria.";";
              $html .= number_format($result->preco,2,',','').";";
              $html .= $result->semana.";";
              $html .= $result->date_box.";";
              $html .= $result->quantidade.";";
              $html .= $result->liberacao.";";
              $html .= "\n";


            }

            fwrite($myfile, $html);

            $this->line('Escreveu CSB '.date('d-m-Y H:i:s'));

            $sql = "SELECT
            b.*,
            (SELECT max(lvo.lv_confirm_picking_id) lv_confirm_picking_id
            FROM
            lv_confirm_picking_orders lvo
            join lv_confirm_picking lv on lv.lv_confirm_picking_id = lvo.lv_confirm_picking_id
            where lvo.type_system = 'lvb'
            and lv.active = 1
            and lv.is_admin = 0
            and lvo.active = 1
            and lvo.order_id = b.pedido limit 1) as liberacao
            FROM vw_report_product_box_lojavirtual_balone b ";

            $results_lojavirtual =  DB::select($sql);

            $this->line('Terminou query do LVB '.date('d-m-Y H:i:s'));

            $html = "";


            foreach ($results_lojavirtual as $result) {

              $html .= $result->plataforma.";";
              $html .= date("d/m/Y",strtotime($result->data)).";";
              $html .= $result->pedido.";";
              $html .= $result->area.";";
              $html .= $result->loja.";";
              $html .= $result->modelo.";";
              $html .= $result->brazil_code.";";
              $html .= $result->categoria.";";
              $html .= number_format($result->preco,2,',','').";";
              $html .= $result->semana.";";
              $html .= $result->date_box.";";
              $html .= $result->quantidade.";";
              $html .= $result->liberacao.";";
              $html .= "\n";



            }

            $this->line('Escreveu LVB '.date('d-m-Y H:i:s'));

            fwrite($myfile, $html);


            fclose($myfile);

          }
        }
