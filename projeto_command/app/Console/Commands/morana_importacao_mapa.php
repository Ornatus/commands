<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class morana_importacao_mapa extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:morana-importacao-mapa';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Command description';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    $file = env('PATH_REPORT_FILE')."\importacao_mapa.csv";
    $data = array();
    $num_linha = 1;
    $insert = "";
    $contador = 0;
    $lines = 0;

    if(isset($file)){

      $ponteiro = fopen($file, "r");

      while (!feof($ponteiro)) {
        $linha = fgets($ponteiro, 4096);


        if($lines ==0){

          $index = 11;
          $num_coluna = 0;
          $colunas = explode(";", $linha);

          foreach ($colunas as $coluna) {
            if($num_coluna >= $index){
              $ano = trim(str_replace("ANO","",$coluna));
              $ano_index[$index] = $ano;
              $index++;
            }
            $num_coluna++;
          }

          $data['years'] = $ano_index;

        }


        if (!empty(trim($linha)) && !preg_match('@(;;;;;;;;;;;)@is',$linha)) {
          if($lines !=0){

            $this->line('Linha: '.$linha.' '.date('d-m-Y H:i:s'));

            $collums = explode(";", $linha);

            $dados = array();
            $dados2 = array();

            $dados['codigo'] 		  = isset($collums[0]) && $collums[0] != ''  ? addslashes($collums[0]) : 0;
            $dados['descricao'] 	= isset($collums[1]) && $collums[1] != '' ? addslashes($collums[1]) : 0.00; ;
            $dados['codforn'] 		= isset($collums[2]) && $collums[2] != 0  ? preg_replace('(\D+)', '', $collums[2]) : 0;
            $dados['desctipo'] 	  = isset($collums[3]) && $collums[3] != '' ? addslashes($collums[3]) : 0.00; ;
            $dados['pr_venda'] 	  = isset($collums[4]) && $collums[4] != 0.00 ? $collums[4] : 0.00;
            $dados['lojas_cad'] 	= isset($collums[5]) && $collums[5] != 0 ? preg_replace('(\D+)', '', $collums[5]) : 0;
            $dados['lojas'] 			= isset($collums[6]) && $collums[6] != 0 ? preg_replace('(\D+)', '', $collums[6]) : 0;
            $dados['stq_lojas'] 	= isset($collums[7]) && $collums[7] != 0 ? preg_replace('(\D+)', '', $collums[7]) : 0;
            $dados['stq_logis'] 	= isset($collums[8]) && $collums[8] != 0 ? preg_replace('(\D+)', '', $collums[8]) : 0;
            $dados['qtdesai'] 		= isset($collums[9]) && $collums[9] != 0 ? preg_replace('(\D+)', '', $collums[9]) : 0;
            $dados['qtdcompras'] 	= isset($collums[10]) && $collums[10] != 0 ? preg_replace('(\D+)', '', $collums[10]) : 0;

            $insert[] = $dados;


            foreach ($data['years'] as $key => $value) {
            $year_value = 	isset($collums[$key]) && $collums[$key] != 0 ? preg_replace('(\D+)', '', $collums[$key]) : 0;

            $dados2['ano'] = $year_value;
            $dados2['valor'] = $value;
            $dados2['codigo'] = $dados['codigo'];

            $insert2[] = $dados2;

          }

        }
        $lines++;

        if($contador == 50){
          $this->line('Inserindo no BANCO 1   '.date('d-m-Y H:i:s'));
          DB::table('mapa.mapa_produto')->insert($insert);
            $this->line('Inserindo no BANCO 2    '.date('d-m-Y H:i:s'));
          DB::table('mapa.mapa_produto_clone')->insert($insert);
            $this->line('Inserindo no BANCO 3    '.date('d-m-Y H:i:s'));
          DB::table('mapa.mapa_produto_vendas_ano')->insert($insert2);
          $this->line('FIIIIIIIIIIM        '.date('d-m-Y H:i:s'));
					$insert="";
					$contador = 0;
				}else{
					$contador++;
				}

      }
    }

    fclose($ponteiro);


  }

}



public function insertFile($data)
{

  $sql = "INSERT INTO mapa.mapa_produto
  SET
  codigo = '".$data['codigo']."',
  descricao = '".$data['descricao']."',
  codforn = '".$data['codforn']."',
  desctipo = '".$data['desctipo']."',
  pr_venda = '".$data['pr_venda']."',
  lojas_cad = '".$data['lojas_cad']."',
  lojas = '".$data['lojas']."',
  stq_lojas = '".$data['stq_lojas']."',
  stq_logis = '".$data['stq_logis']."',
  qtdesai = '".$data['qtdesai']."',
  qtdcompras = '".$data['qtdcompras']."' ; ";

  //	$query = $this->db->query($sql);

  return $sql;

}


public function insertYearsValuesByCode($code,$year_value,$year)
{

  $sql = "INSERT INTO mapa.mapa_produto_vendas_ano
  SET
  codigo = '".$code."',
  valor = '".$year_value."',
  ano = '".$year."' ;";

  //$query = $this->db->query($sql);

  return $sql;

}
}
