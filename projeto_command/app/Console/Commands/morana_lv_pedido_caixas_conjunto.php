<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;

class morana_lv_pedido_caixas_conjunto extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:morana-lv-pedido-caixas-conjunto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Faz os pedidos de caixas de conjunto';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $lojas[1]=60;
      $lojas[10]=300;
      $lojas[14]=100;
      $lojas[20]=70;
      $lojas[22]=100;
      $lojas[24]=100;
      $lojas[26]=60;
      $lojas[30]=40;
      $lojas[31]=80;
      $lojas[32]=150;
      $lojas[38]=5;
      $lojas[44]=350;
      $lojas[47]=30;
      $lojas[58]=100;
      $lojas[61]=60;
      $lojas[66]=100;
      $lojas[75]=200;
      $lojas[80]=5;
      $lojas[81]=30;
      $lojas[85]=100;
      $lojas[99]=70;
      $lojas[109]=60;
      $lojas[110]=30;
      $lojas[115]=350;
      $lojas[116]=250;
      $lojas[119]=300;
      $lojas[123]=60;
      $lojas[292]=100;
      $lojas[130]=20;
      $lojas[132]=10;
      $lojas[134]=400;
      $lojas[345]=700;
      $lojas[145]=20;
      $lojas[148]=300;
      $lojas[151]=20;
      $lojas[157]=200;
      $lojas[161]=30;
      $lojas[165]=10;
      $lojas[167]=30;
      $lojas[174]=96;
      $lojas[288]=300;
      $lojas[183]=70;
      $lojas[189]=200;
      $lojas[190]=20;
      $lojas[296]=100;
      $lojas[207]=100;
      $lojas[208]=150;
      $lojas[297]=50;
      $lojas[298]=200;
      $lojas[224]=100;
      $lojas[226]=620;
      $lojas[227]=300;
      $lojas[228]=25;
      $lojas[233]=100;
      $lojas[234]=40;
      $lojas[242]=50;
      $lojas[303]=50;
      $lojas[312]=30;
      $lojas[108]=250;
      $lojas[314]=30;
      $lojas[317]=25;
      $lojas[320]=30;
      $lojas[330]=30;
      $lojas[331]=130;
      $lojas[332]=10;
      $lojas[377]=50;
      $lojas[371]=500;
      $lojas[357]=10;
      $lojas[355]=300;
      $lojas[184]=300;
      $lojas[349]=60;
      $lojas[379]=200;
      $lojas[361]=100;
      $lojas[378]=100;
      $lojas[348]=120;
      $lojas[391]=30;
      $lojas[375]=100;
      $lojas[84]=30;
      $lojas[300]=100;
      $lojas[401]=50;
      $lojas[403]=5;
      $lojas[310]=150;
      $lojas[376]=26;
      $lojas[414]=100;
      $lojas[106]=250;
      $lojas[203]=130;
      $lojas[344]=50;
      $lojas[182]=30;
      $lojas[103]=50;
      $lojas[188]=40;
      $lojas[62]=30;
      $lojas[429]=30;
      $lojas[430]=100;
      $lojas[433]=30;
      $lojas[442]=50;
      $lojas[441]=30;
      $lojas[445]=55;
      $lojas[381]=50;
      $lojas[449]=40;
      $lojas[446]=150;
      $lojas[411]=10;
      $lojas[448]=50;
      $lojas[452]=20;
      $lojas[111]=35;
      $lojas[462]=60;
      $lojas[463]=10;
      $lojas[467]=60;
      $lojas[465]=20;
      $lojas[466]=10;
      $lojas[468]=350;
      $lojas[469]=100;
      $lojas[470]=10;
      $lojas[50]=100;
      $lojas[437]=4;
      $lojas[438]=100;
      $lojas[386]=250;
      $lojas[453]=100;
      $lojas[454]=50;
      $lojas[440]=100;
      $lojas[435]=50;
      $lojas[456]=150;
      $lojas[451]=60;
      $lojas[477]=50;
      $lojas[436]=100;
      $lojas[481]=70;
      $lojas[482]=40;
      $lojas[484]=250;
      $lojas[485]=230;
      $lojas[150]=50;
      $lojas[475]=100;
      $lojas[488]=50;
      $lojas[491]=50;
      $lojas[425]=150;


      foreach($lojas as $loja=>$qtd)
      {
        $order = 0;

        $insert = "INSERT INTO lv_order
                    SET
                    customer_id = '".$loja."',
                    status_id = 1,
                    type_user_id=1,
                    send_mail=1,
                    is_material=1";

                    DB::insert($insert);

        $order = DB::getPdo()->lastInsertId();

        $op = "INSERT INTO lv_order_product
                SET
                lv_order_id = '".$order."',
                product_id = 145461,
                fixed_price=10,
                suggested_price=19.90,
                price=10,
                ativo=1,
                quantity='".$qtd."';";

                DB::insert($op);
      }
    }
}
