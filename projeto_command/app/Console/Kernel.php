<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
      Commands\balone_csb_adiciona_produtos_carrinho_lb::class,
      Commands\balone_csb_adiciona_pedidos_automatico_lb_master::class,
      Commands\balone_csb_adicionar_carrinho_balone::class,
      Commands\balone_csb_adiciona_pedidos_automatico_balone::class,
      Commands\balone_csb_adiciona_produtos_carrinho_lb_fase_2::class,
      Commands\morana_csm_adiciona_produtos_carrinho::class,
      Commands\morana_csm_adiciona_pedidos_automatico::class,
      Commands\morana_itens_caixa::class,
      Commands\balone_itens_caixa::class,
      Commands\morana_csm_imagens_semana::class,
      Commands\balone_csb_imagens_semana::class,
      Commands\balone_lv_atualiza_estoque::class,
      Commands\morana_ppm_atualiza_pedidos_faturados::class,
      Commands\morana_sga_envia_email_alteracao_preco_produto::class,
      Commands\morana_csm_email_itens_caixa::class,
      Commands\morana_mapa_historico_produtos_caixa::class,
      Commands\morana_csm_thumb_imagens_carrinho::class,
      Commands\morana_csm_exclui_imagens::class,
      Commands\balone_csb_thumb_imagens_carrinho::class,
      Commands\balone_csb_exclui_imagens::class,
      Commands\morana_itens_caixa_ecommerce::class,
      Commands\morana_csm_split_pedido_162::class,
      Commands\morana_lista_imagens_ecommerce::class,
      Commands\morana_csm_caixa_historico_semana::class,
      Commands\morana_csm_email_historico_semana::class,
      Commands\morana_csm_caixa_historico_semana_range::class,
      Commands\morana_csm_deleta_arquivo_caixa_historico_semana_range::class,
      Commands\suporte_deleta_backup::class,
      Commands\morana_importacao_mapa::class,
      Commands\morana_csm_email_prioridade_limite_liberacao::class,
      Commands\morana_csm_email_prioridade_consultor::class,
      Commands\balone_csb_caixa_historico_semana::class,
      Commands\balone_csb_historico_semana_range::class,
      Commands\morana_lv_desabilita_produtos_campanha::class,
      Commands\balone_lb_importacao_vendas::class,
      Commands\morana_lv_pedido_caixas_conjunto::class,
      Commands\morana_csm_importa_produtos_semana::class,
      Commands\morana_lv_cancela_pedidos_pendentes_vencidos::class,
      Commands\morana_lv_envia_email_pedidos_pendentes::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
